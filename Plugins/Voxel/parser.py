import shutil
import os
import sys
from glob import glob


def remove_empty_folders(path, remove_root=True):
    assert os.path.isdir(path)

    # remove empty subfolders
    files = os.listdir(path)
    if len(files):
        for f in files:
            fullpath = os.path.join(path, f)
            if os.path.isdir(fullpath):
                remove_empty_folders(fullpath)

    # if folder empty, delete it
    files = os.listdir(path)
    if len(files) == 0 and remove_root:
        print("Removing empty folder:", path)
        os.rmdir(path)


def get_include(path, line):
    if line.lstrip().startswith('#include'):
        parts = line.split('"')
        if len(parts) > 1:
            assert len(parts) == 3 and (parts[2] in ["", "\n"] or parts[2].strip().startswith("//")), "Invalid include: {} in {}\n{}".format(line, path, parts)
            return parts[1].split("/")[-1]
    return None


current_file = ""
current_mode = ""
line_number = 0
current_line = ""


def check(value, error):
    assert len(current_file) > 0
    assert value, "{}:{}\ncurrent mode={}\n{}\n".format(
        current_file,
        line_number,
        current_mode,
        current_line) + error


assert len(sys.argv) == 3, "parse.py free|pro source"

mode = sys.argv[1].lower()
assert mode in ["free", "pro"], "Invalid mode: " + mode
source_path = sys.argv[2] + "\\Source"
assert os.path.isdir(source_path), "Invalid source path"

print("Mode: " + mode)
print("Source path: " + source_path)

source_files = []
for path, subdirs, files in os.walk(source_path):
    for file in files:
        file_path = os.path.join(source_path, path, file)
        filename, file_extension = os.path.splitext(file_path)
        if file_extension in [".h", ".cpp", ".cs"]:
            source_files.append(file_path)
        else:
            print("Ignoring " + file_path)

for file in source_files:
    print("Parsing {}".format(file))

    current_file = file
    tmp_file = file + "__parser__file__.txt"
    current_mode = "all"

    with open(file) as old, open(tmp_file, 'w') as new:
        line_number = 1
        preprocessor_depth = 0
        currentif_depth = -1
        for line in old:
            real_line = line
            if line[-1] == '\n':
                line = line[:-1]
            line = line.strip()
            current_line = line

            if line_number == 1 and "embree-3.5.2" not in file:
                check(line == "// Copyright 2020 Phyronnaz", "Invalid copyright!")

            if line == "#if VOXEL_PLUGIN_PRO" or line == "#if 1 // VOXEL_PLUGIN_PRO":
                check(current_mode == "all", "Invalid #if VOXEL_PLUGIN_PRO")
                current_mode = "pro"
                preprocessor_depth += 1
                currentif_depth = preprocessor_depth

            elif line == "#if !VOXEL_PLUGIN_PRO" or line == "#if 0 // !VOXEL_PLUGIN_PRO":
                check(current_mode == "all", "Invalid #if !VOXEL_PLUGIN_PRO")
                current_mode = "free"
                preprocessor_depth += 1
                currentif_depth = preprocessor_depth

            elif line == "#else" and preprocessor_depth == currentif_depth:
                check(current_mode != "all", "Invalid #else")
                current_mode = "pro" if current_mode == "free" else "free"

            elif line == "#endif" and preprocessor_depth == currentif_depth:
                check(current_mode != "all", "Invalid #endif")
                current_mode = "all"
                preprocessor_depth -= 1
                currentif_depth = -1
                check(preprocessor_depth >= 0, "Invalid #endif")

            elif line == "bFasterWithoutUnity = true;":
                pass # Skip this line when packaging

            else:
                check("VOXEL_PLUGIN_PRO" not in line or ".Build.cs" in file, "Invalid VOXEL_PLUGIN_PRO!")
                
                if line.strip().startswith("#if"):
                    preprocessor_depth += 1

                if line.strip().startswith("#endif"):
                    preprocessor_depth -= 1
                    check(preprocessor_depth >= 0, "Invalid #endif")

                if current_mode == "all" or current_mode == mode:
                    new.write(real_line)
            line_number += 1
        check(current_mode == "all", "Missing #endif at end of file")

    shutil.move(tmp_file, file)

modified_count = 1

while modified_count > 0:
    modified_count = 0

    includes_to_remove = []

    for file in source_files:
        if os.path.isfile(file):
            name = os.path.basename(file)
            delete = True
            with open(file) as f:
                for line in f:
                    if len(line) > 1 and not line.startswith("//") and not line.startswith("#pragma once"):
                        delete = False
                        break

            if delete:
                print("Deleting {} ({})".format(file, name))
                includes_to_remove.append(name)
                os.remove(file)

    for file in source_files:
        if os.path.isfile(file):
            print("Parsing {}".format(file))

            tmp_file = file + "__parser__file__.txt"

            with open(file) as old, open(tmp_file, 'w') as new:
                for line in old:
                    include = get_include(file, line)
                    if include is None or include not in includes_to_remove:
                        new.write(line)
                    else:
                        print("Removing {}".format(line))
                        modified_count += 1

            shutil.move(tmp_file, file)

remove_empty_folders(source_path)

print("Success!")
