// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"

class FVoxelGraphErrorReporter;
class FVoxelGraph;

namespace FVoxelGraphChecker
{
	void CheckGraph(FVoxelGraphErrorReporter& ErrorReporter, const FVoxelGraph& Graph);
}
#endif