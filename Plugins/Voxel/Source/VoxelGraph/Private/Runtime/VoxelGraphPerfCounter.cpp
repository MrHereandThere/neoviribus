// Copyright 2020 Phyronnaz

#if VOXEL_PLUGIN_PRO
#include "Runtime/VoxelGraphPerfCounter.h"

FCriticalSection FVoxelGraphPerfCounter::Section;
FVoxelGraphPerfCounter::FNodePerfTree FVoxelGraphPerfCounter::SingletonTree;

FCriticalSection FVoxelGraphRangeFailuresReporter::Section;
FVoxelGraphRangeFailuresReporter::FNodeErrorMap FVoxelGraphRangeFailuresReporter::SingletonNodes;

#endif