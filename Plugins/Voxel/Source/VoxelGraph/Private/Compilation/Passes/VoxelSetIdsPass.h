// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"
#include "VoxelCompilationPass.h"

struct FVoxelSetPinsIdsPass
{
	VOXEL_PASS_BODY(FVoxelSetPinsIdsPass);
	
	static void Apply(FVoxelGraphCompiler& Compiler, int32& Id);
};

struct FVoxelSetFunctionsIdsPass
{
	VOXEL_PASS_BODY(FVoxelSetFunctionsIdsPass);
	
	static void Apply(FVoxelGraphCompiler& Compiler);
};
#endif