// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"
#include "VoxelCompilationPass.h"

struct FVoxelReplaceBiomeMergePass
{
	VOXEL_PASS_BODY(FVoxelReplaceBiomeMergePass);

	static void Apply(FVoxelGraphCompiler& Compiler);
};

#endif