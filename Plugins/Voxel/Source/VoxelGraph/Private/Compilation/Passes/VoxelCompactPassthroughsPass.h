// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"
#include "VoxelCompilationPass.h"

struct FVoxelCompactPassthroughsPass
{
	VOXEL_PASS_BODY(FVoxelCompactPassthroughsPass);

	static void Apply(FVoxelGraphCompiler& Compiler);
};
#endif
