// Copyright 2020 Phyronnaz

#include "VoxelTools/VoxelToolManagerTools.h"
#include "VoxelTools/VoxelSurfaceTools.h"
#include "VoxelTools/VoxelSphereTools.h"
#include "VoxelTools/VoxelProjectionTools.h"
#include "VoxelTools/VoxelBlueprintLibrary.h"
#include "VoxelTools/VoxelHardnessHandler.h"
#include "VoxelTools/VoxelAssetTools.h"
#include "VoxelTools/VoxelDataTools.h"
#include "VoxelTools/VoxelDataTools.inl"
#include "VoxelTools/VoxelWorldGeneratorTools.h"
#include "VoxelTools/VoxelToolManagerToolsHelpers.h"
#include "VoxelRender/VoxelMaterialInterface.h"
#include "VoxelRender/IVoxelLODManager.h"
#include "VoxelRender/VoxelProceduralMeshComponent.h"
#include "VoxelSpawners/VoxelSpawnerManager.h"
#include "VoxelMessages.h"
#include "VoxelThreadingUtilities.h"
#include "VoxelDebugUtilities.h"
#include "VoxelWorld.h"

#include "VoxelData/VoxelData.h"

#include "Materials/MaterialInstanceDynamic.h"
#include "DrawDebugHelpers.h"
#include "Engine/Engine.h"
#include "Engine/StaticMesh.h"
#include "Engine/StaticMeshActor.h"
#include "Components/StaticMeshComponent.h"

FVoxelToolManagerTool::FVoxelToolManagerTool(const UVoxelToolManager& ToolManager, const FToolSettings& ToolSettings)
	: ToolManager(ToolManager)
	, ToolSettings(ToolSettings)
{
}

FVoxelToolManagerTool::~FVoxelToolManagerTool()
{
	ClearVoxelWorld();
}

void FVoxelToolManagerTool::TriggerTick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData, const FHitResult& Hit)
{
	VOXEL_FUNCTION_COUNTER();
	
	if (&World != VoxelWorld) ClearVoxelWorld();
	VoxelWorld = &World;

	if (ToolManager.bWaitForUpdates && NumPendingUpdates > 0)
	{
		if (ToolManager.bDebug)
		{
			GEngine->AddOnScreenDebugMessage(
				OBJECT_LINE_ID(),
				1.5f * TickData.DeltaTime,
				FColor::Yellow,
				FString::Printf(TEXT("Waiting for %d updates"), NumPendingUpdates));
		}
		return;
	}

	MouseMovementSize = (LastFrameTickData.MousePosition - TickData.MousePosition).Size();
	bCanEdit = TickData.bClick;

	// Update position/normal to the hit ones
	if (Hit.bBlockingHit)
	{
		CurrentPosition = Hit.ImpactPoint;
		CurrentNormal = Hit.ImpactNormal;
	}
	
	if (ToolSettings.bViewportSpaceMovement)
	{
		if (*ToolSettings.Alignment == EVoxelToolManagerAlignment::Surface)
		{
			UVoxelProceduralMeshComponent::SetVoxelCollisionsFrozen(TickData.bClick);

			// No need to override position/normal
		}
		else
		{
			if (!LastFrameTickData.bClick)
			{
				FVector Normal = FVector::UpVector;
				switch (*ToolSettings.Alignment)
				{
				case EVoxelToolManagerAlignment::View:
					Normal = -TickData.CameraViewDirection;
					break;
				case EVoxelToolManagerAlignment::Ground:
					Normal = FVector::UpVector;
					break;
				case EVoxelToolManagerAlignment::Up:
					Normal = -TickData.CameraViewDirection;
					Normal.Z = 0;
					if (!Normal.Normalize())
					{
						ensure(TickData.CameraViewDirection.GetAbs() == FVector::UpVector);
						Normal = FVector::RightVector;
					}
					break;
				default: ensure(false);
				}

				const bool bAirMode = !Hit.bBlockingHit || *ToolSettings.bAirMode;
				const FVector Point = bAirMode
					? TickData.RayOrigin + TickData.RayDirection * *ToolSettings.DistanceToCamera
					: FVector(Hit.ImpactPoint);
				ViewportSpaceMovement.LastClickPlane = FPlane(Point, Normal);
				ViewportSpaceMovement.LastClickNormal = bAirMode ? FVector::UpVector : Hit.ImpactNormal;
			}

			UVoxelProceduralMeshComponent::SetVoxelCollisionsFrozen(false);
			
			// Override position/normal
			CurrentPosition = FMath::RayPlaneIntersection(TickData.RayOrigin, TickData.RayDirection, ViewportSpaceMovement.LastClickPlane);
			CurrentNormal = ViewportSpaceMovement.LastClickNormal;
		}
	}

	if (ToolSettings.bSupportToolDirection)
	{
		const FVector NewMovementTangent = (CurrentPosition - LastPositionUsedForTangent).GetSafeNormal();
		MovementTangent = FMath::Lerp(
			MovementTangent,
			NewMovementTangent,
			FMath::Clamp((1 - ToolManager.AlignToMovementSmoothness) * GetMouseMovementSize() / 10, 0.f, 1.f)).GetSafeNormal();
		LastPositionUsedForTangent = CurrentPosition;
	}

	if (ToolSettings.bFixedNormal && *ToolSettings.bFixedNormal)
	{
		CurrentNormal = ToolSettings.FixedNormal;
	}

	if (ToolSettings.bSupportStride && *ToolSettings.bEnableStride)
	{
		if (!LastFrameTickData.bClick || // If not clicking always keep the position under the cursor
			FVector::Dist(CurrentPosition, StridePosition) >= *ToolSettings.Stride * 2 * ToolManager.Radius) // 2x : we want the diameter
		{
			StridePosition = CurrentPosition;
			StrideNormal = CurrentNormal;
			if (ToolSettings.bSupportToolDirection)
			{
				StrideDirection = MovementTangent;
			}
		}
		else
		{
			bCanEdit = false;
		}
	}

	if (!TickData.bClick && PendingFrameBounds.IsValid())
	{
		ToolManager.SaveFrame(World, PendingFrameBounds.GetBox(), ToolSettings.ToolName);

#if VOXEL_PLUGIN_PRO
		if (ToolManager.bRegenerateSpawners)
		{
			if (auto* SpawnerManager = World.GetSpawnerManager())
			{
				SpawnerManager->Regenerate(PendingFrameBounds.GetBox());
			}
		}
#endif

		if (ToolManager.bCheckForSingleValues)
		{
			UVoxelDataTools::CheckForSingleValues(&World, PendingFrameBounds.GetBox());
			UVoxelDataTools::CheckForSingleMaterials(&World, PendingFrameBounds.GetBox());
		}

		if (ToolManager.bDebug)
		{
			UVoxelDebugUtilities::DrawDebugIntBox(&World, PendingFrameBounds.GetBox(), 0.5f, 0, FColor::Purple);
		}
		
		PendingFrameBounds.Reset();
	}

	if (ToolManager.bDebug)
	{
		if (ToolSettings.bSupportToolDirection)
		{
			DrawDebugDirectionalArrow(
				TickData.World,
				GetToolPosition(),
				GetToolPosition() + GetToolDirection() * World.VoxelSize * 5,
				World.VoxelSize * 5,
				FColor::Red,
				false,
				1.5f * TickData.DeltaTime,
				0,
				World.VoxelSize / 2);
		}
		if (ToolSettings.bViewportSpaceMovement && TickData.bClick && *ToolSettings.Alignment != EVoxelToolManagerAlignment::Surface)
		{
			DrawDebugSolidPlane(
				TickData.World,
				ViewportSpaceMovement.LastClickPlane,
				CurrentPosition,
				1000000,
				FColor::Red,
				false,
				1.5f * TickData.DeltaTime);
		}
		DrawDebugDirectionalArrow(
			TickData.World,
			GetToolPosition(),
			GetToolPosition() + GetToolNormal() * World.VoxelSize * 5,
			World.VoxelSize * 5,
			FColor::Blue,
			false,
			1.5f * TickData.DeltaTime,
			0,
			World.VoxelSize / 2);
	}
	
	if (ToolSettings.bNeedToolRendering)
	{
		auto& ToolRenderingManager = World.GetToolRenderingManager();
		if (!ToolRenderingId.IsValid() || !ToolRenderingManager.IsValidTool(ToolRenderingId))
		{
			ToolRenderingId = ToolRenderingManager.CreateTool(true);
		}

		auto* const ToolMaterial = *ToolSettings.ToolMaterial;
		if (!ToolMaterial)
		{
			FVoxelMessages::Error("VoxelToolManager: " + ToolSettings.ToolName.ToString() + ": Invalid ToolMaterial!");
			return;
		}
		if (!ToolMaterialInstance || ToolMaterialInstance->Parent != ToolMaterial)
		{
			ToolMaterialInstance = UMaterialInstanceDynamic::Create(ToolMaterial, GetTransientPackage());
			if (!ensure(ToolMaterialInstance)) return;
		}
		check(ToolMaterialInstance);
		
		ToolRenderingManager.EditTool(ToolRenderingId, [&](FVoxelToolRendering& Tool)
		{
			if (!Tool.Material.IsValid() || Tool.Material->GetMaterial() != ToolMaterialInstance) 
			{
				Tool.Material = FVoxelMaterialInterface::Create(ToolMaterialInstance);
			}
		});
	}

	if (ToolSettings.bNeedToolMesh && StaticMeshActor.IsValid())
	{
		auto& MeshComponent = *StaticMeshActor->GetStaticMeshComponent();
		if (MeshComponent.GetStaticMesh() != *ToolSettings.ToolMesh)
		{
			MeshComponent.SetStaticMesh(*ToolSettings.ToolMesh);
			for (int32 Index = 0; Index < MeshComponent.GetNumMaterials(); Index++)
			{
				MeshComponent.SetMaterial(Index, *ToolSettings.ToolMeshMaterial);
			}
		}
	}

	Tick(World, TickData);

	LastFrameTickData = TickData;
	bLastFrameCanEdit = bCanEdit;
}

void FVoxelToolManagerTool::ClearVoxelWorld()
{
	VOXEL_FUNCTION_COUNTER();
	
	if (VoxelWorld.IsValid() && VoxelWorld->IsCreated())
	{
		if (PendingFrameBounds.IsValid())
		{
			ToolManager.SaveFrame(*VoxelWorld, PendingFrameBounds.GetBox(), ToolSettings.ToolName);
			PendingFrameBounds.Reset();
		}
		if (ToolRenderingId.IsValid())
		{
			auto& ToolRenderingManager = VoxelWorld->GetToolRenderingManager();
			if (ToolRenderingManager.IsValidTool(ToolRenderingId)) // Could be invalid if the voxel world was toggled off & on
			{
				ToolRenderingManager.RemoveTool(ToolRenderingId);
			}
		}
	}
	VoxelWorld.Reset();
	ToolRenderingId.Reset();

	if (StaticMeshActor.IsValid())
	{
		StaticMeshActor->Destroy();
	}

	UVoxelProceduralMeshComponent::SetVoxelCollisionsFrozen(false);
}

///////////////////////////////////////////////////////////////////////////////

FVector FVoxelToolManagerTool::GetToolPosition() const
{
	return ToolSettings.bSupportStride && *ToolSettings.bEnableStride ? StridePosition : CurrentPosition;
}

FVector FVoxelToolManagerTool::GetToolPreviewPosition() const
{
	// Ignore stride for preview
	return CurrentPosition;
}

FVector FVoxelToolManagerTool::GetToolNormal() const
{
	return ToolSettings.bSupportStride && *ToolSettings.bEnableStride ? StrideNormal : CurrentNormal;
}

FVector FVoxelToolManagerTool::GetToolDirection() const
{
	ensure(ToolSettings.bSupportToolDirection);
	if (!ToolSettings.bAlignToMovement || *ToolSettings.bAlignToMovement)
	{
		return ToolSettings.bSupportStride && *ToolSettings.bEnableStride ? StrideDirection : MovementTangent;
	}
	else
	{
		return ToolSettings.Direction->Vector();
	}
}

UMaterialInstanceDynamic& FVoxelToolManagerTool::GetToolMaterialInstance() const
{
	check(ToolSettings.bNeedToolRendering);
	check(ToolMaterialInstance);
	return *ToolMaterialInstance;
}

void FVoxelToolManagerTool::UpdateWorld(AVoxelWorld& World, const FIntBox& Bounds)
{
	NumPendingUpdates += World.GetLODManager().UpdateBounds_OnAllFinished(
		Bounds,
		FVoxelUtilities::MakeVoxelWeakPtrDelegate(this, 
		[VoxelWorld = MakeWeakObjectPtr(&World), Bounds](FVoxelToolManagerTool& This)
		{
			if (VoxelWorld.IsValid())
			{
				This.ToolManager.OnBoundsUpdated.Broadcast(VoxelWorld.Get(), Bounds);
				if (This.ToolManager.bDebug)
				{
					UVoxelDebugUtilities::DrawDebugIntBox(VoxelWorld.Get(), Bounds, FTransform(), 0.1f);
				}
			}
		}),
		FVoxelUtilities::MakeVoxelWeakPtrDelegate<void, FIntBox>(this, 
		[](FVoxelToolManagerTool& This, const FIntBox& ChunkBounds)
		{
			ensure(This.NumPendingUpdates-- >= 0);
		}));
}

void FVoxelToolManagerTool::SaveFrameOnEndClick(const FIntBox& Bounds)
{
	PendingFrameBounds += Bounds;
}

void FVoxelToolManagerTool::SetToolRenderingBounds(AVoxelWorld& World, const FBox& Bounds)
{
	if (!ensure(&World == VoxelWorld)) return;
	World.GetToolRenderingManager().EditTool(ToolRenderingId, [&](FVoxelToolRendering& Tool)
	{
		Tool.WorldBounds = Bounds;
	});
}

void FVoxelToolManagerTool::SetToolMeshTransform(UWorld* World, const FTransform& Transform)
{
	VOXEL_FUNCTION_COUNTER();

	ensure(ToolSettings.bNeedToolMesh);
	if (!StaticMeshActor.IsValid())
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.bDeferConstruction = true;
		SpawnParameters.ObjectFlags = RF_Transient;
		StaticMeshActor = World->SpawnActor<AStaticMeshActor>(SpawnParameters);
		StaticMeshActor->SetMobility(EComponentMobility::Movable);
#if WITH_EDITOR
		StaticMeshActor->SetActorLabel("VoxelToolMeshActor");
#endif
		auto& MeshComponent = *StaticMeshActor->GetStaticMeshComponent();
		MeshComponent.SetStaticMesh(*ToolSettings.ToolMesh);
		for (int32 Index = 0; Index < MeshComponent.GetNumMaterials(); Index++)
		{
			MeshComponent.SetMaterial(Index, *ToolSettings.ToolMeshMaterial);
		}
		StaticMeshActor->SetActorEnableCollision(false);
		StaticMeshActor->FinishSpawning(Transform);
	}
	if (!ensure(StaticMeshActor.IsValid())) return;
	StaticMeshActor->SetActorTransform(Transform);
}

FIntBox FVoxelToolManagerTool::GetAndDebugBoundsToCache(AVoxelWorld& World, const FIntBox& Bounds, const FVoxelToolManagerTickData& TickData) const
{
	const auto BoundsToCache = UVoxelBlueprintLibrary::GetRenderBoundsOverlappingDataBounds(&World, Bounds);

	if (ToolManager.bDebug)
	{
		UVoxelDebugUtilities::DrawDebugIntBox(&World, Bounds, 1.5f * TickData.DeltaTime, 0, FColor::Green);
		UVoxelDebugUtilities::DrawDebugIntBox(&World, BoundsToCache, 1.5f * TickData.DeltaTime, 0, FColor::Red);
	}
	
	return BoundsToCache;
}

///////////////////////////////////////////////////////////////////////////////

void FVoxelToolManagerTool::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(ToolMaterialInstance);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

inline FVoxelToolManagerTool::FToolSettings SurfaceToolSettings(const FVoxelToolManager_SurfaceSettings& SurfaceSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Surface Tool");
	
	Settings.bSupportStride = true;
	Settings.bEnableStride = &SurfaceSettings.bEnableStride;
	Settings.Stride = &SurfaceSettings.Stride;
	
	Settings.bNeedToolRendering = true;
	Settings.ToolMaterial = &SurfaceSettings.ToolMaterial;

	Settings.bSupportToolDirection = true;
	Settings.bAlignToMovement = &SurfaceSettings.bAlignToMovement;
	Settings.Direction = &SurfaceSettings.FixedDirection;

	Settings.bFixedNormal = &SurfaceSettings.b2DBrush;
	Settings.FixedNormal = FVector::UpVector;
	
	return Settings;
}

inline FVoxelToolManagerTool::FToolSettings FlattenToolSettings(const FVoxelToolManager_FlattenSettings& FlattenSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Flatten Tool");
	
	Settings.bNeedToolRendering = true;
	Settings.ToolMaterial = &FlattenSettings.ToolMaterial;
	
	return Settings;
}

inline FVoxelToolManagerTool::FToolSettings TrimToolSettings(const FVoxelToolManager_TrimSettings& TrimSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Trim Tool");

	Settings.bNeedToolRendering = true;
	Settings.ToolMaterial = &TrimSettings.ToolMaterial;

	return Settings;
}

inline FVoxelToolManagerTool::FToolSettings LevelToolSettings(const FVoxelToolManager_LevelSettings& LevelSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Level Tool");
	
	Settings.bSupportStride = true;
	Settings.bEnableStride = &LevelSettings.bEnableStride;
	Settings.Stride = &LevelSettings.Stride;

	Settings.bNeedToolMesh = true;
	Settings.ToolMesh = &LevelSettings.CylinderMesh;
	Settings.ToolMeshMaterial = &LevelSettings.ToolMaterial;
	
	Settings.bViewportSpaceMovement = true;

	static EVoxelToolManagerAlignment Alignment = EVoxelToolManagerAlignment::Ground;
	static bool bAirMode = false;
	static float DistanceToCamera = 1e5;

	Settings.Alignment = &Alignment;
	Settings.bAirMode = &bAirMode;
	Settings.DistanceToCamera = &DistanceToCamera;

	return Settings;
}

inline FVoxelToolManagerTool::FToolSettings SmoothToolSettings(const FVoxelToolManager_SmoothSettings& SmoothSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Smooth Tool");

	Settings.bNeedToolRendering = true;
	Settings.ToolMaterial = &SmoothSettings.ToolMaterial;

	return Settings;
}

inline FVoxelToolManagerTool::FToolSettings SphereToolSettings(const FVoxelToolManager_SphereSettings& SphereSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Sphere Tool");

	Settings.bNeedToolMesh = true;
	Settings.ToolMesh = &SphereSettings.SphereMesh;
	Settings.ToolMeshMaterial = &SphereSettings.ToolMaterial;
	
	Settings.bViewportSpaceMovement = true;
	Settings.Alignment = &SphereSettings.Alignment;
	Settings.bAirMode = &SphereSettings.bAirMode;
	Settings.DistanceToCamera = &SphereSettings.DistanceToCamera;
	
	return Settings;
}

inline FVoxelToolManagerTool::FToolSettings MeshToolSettings(const FVoxelToolManager_MeshSettings& MeshSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Mesh Tool");
	
	Settings.bSupportStride = true;
	Settings.bEnableStride = &MeshSettings.bEnableStride;
	Settings.Stride = &MeshSettings.Stride;

	Settings.bNeedToolMesh = true;
	Settings.ToolMesh = &MeshSettings.Mesh;
	Settings.ToolMeshMaterial = &MeshSettings.ToolMaterial;

	Settings.bSupportToolDirection = true;
	
	Settings.bViewportSpaceMovement = true;
	Settings.Alignment = &MeshSettings.Alignment;
	Settings.bAirMode = &MeshSettings.bAirMode;
	Settings.DistanceToCamera = &MeshSettings.DistanceToCamera;
	
	return Settings;
}

inline FVoxelToolManagerTool::FToolSettings RevertToolSettings(const FVoxelToolManager_RevertSettings& RevertToGeneratorSettings)
{
	FVoxelToolManagerTool::FToolSettings Settings;

	Settings.ToolName = STATIC_FNAME("Revert Tool");

	Settings.bNeedToolMesh = true;
	Settings.ToolMesh = &RevertToGeneratorSettings.SphereMesh;
	Settings.ToolMeshMaterial = &RevertToGeneratorSettings.ToolMaterial;
	
	Settings.bViewportSpaceMovement = true;
	Settings.Alignment = &RevertToGeneratorSettings.Alignment;
	Settings.bAirMode = &RevertToGeneratorSettings.bAirMode;
	Settings.DistanceToCamera = &RevertToGeneratorSettings.DistanceToCamera;
	
	return Settings;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelToolManagerTool_Surface::FVoxelToolManagerTool_Surface(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, SurfaceToolSettings(ToolManager.SurfaceSettings))
	, SurfaceSettings(ToolManager.SurfaceSettings)
{
}

void FVoxelToolManagerTool_Surface::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();

	struct FMaskData
	{
		TVoxelTexture<float> MaskTexture;
		UTexture2D* MaskRenderTexture = nullptr;
		float MaskScaleX = 0;
		float MaskScaleY = 0;
		FVector MaskPlaneX;
		FVector MaskPlaneY;
		EVoxelRGBA MaskChannel = EVoxelRGBA::R;
	};
	FMaskData MaskData;

	auto& MaterialInstance = GetToolMaterialInstance();
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Radius"), ToolManager.Radius);
	MaterialInstance.SetVectorParameterValue(STATIC_FNAME("Position"), GetToolPreviewPosition());
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Falloff"), SurfaceSettings.Falloff);
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("EnableFalloff"), SurfaceSettings.bEnableFalloff);
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("FalloffType"), int32(SurfaceSettings.FalloffType));
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("UseMask"), false);

	if (SurfaceSettings.bUseMask && (SurfaceSettings.MaskType == EVoxelToolManagerMaskType::Texture 
		? SurfaceSettings.MaskTexture != nullptr 
		: SurfaceSettings.MaskWorldGenerator.IsValid()))
	{
#if VOXEL_PLUGIN_PRO
		if (SurfaceSettings.MaskType == EVoxelToolManagerMaskType::Texture)
		{
			FString Error;
			if (!FVoxelTextureUtilities::CanCreateFromTexture(SurfaceSettings.MaskTexture, Error))
			{
				FVoxelMessages::ShowNotification(
					OBJECT_LINE_ID(),
					Error,
					"Fix Now",
					"Fix the texture",
					FSimpleDelegate::CreateWeakLambda(SurfaceSettings.MaskTexture, [Mask = SurfaceSettings.MaskTexture]() { FVoxelTextureUtilities::FixTexture(Mask); }));
				return;
			}
			MaskData.MaskTexture = FVoxelTextureUtilities::CreateFromTexture_Float(SurfaceSettings.MaskTexture, SurfaceSettings.MaskChannel);
			MaskData.MaskRenderTexture = SurfaceSettings.MaskTexture;
			
			const float MaskWantedSize = 2 * ToolManager.Radius / World.VoxelSize * SurfaceSettings.MaskScale;
			MaskData.MaskScaleX = MaskWantedSize / MaskData.MaskTexture.GetSizeX();
			MaskData.MaskScaleY = MaskWantedSize / MaskData.MaskTexture.GetSizeY() * SurfaceSettings.MaskRatio;
			MaskData.MaskChannel = SurfaceSettings.MaskChannel;
		}
		else
		{
			if (ToolManager.Radius <= 0) return;
			
			bool bNeedToRebuild = false;
			bool bNeedToRebuildSeeds = false;
			if (LastFrameCanEdit() && LastFrameCanEdit() != CanEdit())
			{
				// Reset if we stopped clicking, or if stride edit just ended
				bNeedToRebuild = true;
				bNeedToRebuildSeeds = true;
			}
			if (MaskWorldGeneratorCache.GeneratorObject != SurfaceSettings.MaskWorldGenerator.GetObject() ||
				MaskWorldGeneratorCache.Scale != SurfaceSettings.MaskScale ||
				MaskWorldGeneratorCache.Radius != ToolManager.Radius ||
				MaskWorldGeneratorCache.bScaleWithRadius != SurfaceSettings.bScaleWithRadius)
			{
				bNeedToRebuild = true;
			}
			ensure(!bNeedToRebuildSeeds || bNeedToRebuild);
			
			if (bNeedToRebuild)
			{
				MaskWorldGeneratorCache.GeneratorObject = SurfaceSettings.MaskWorldGenerator.GetObject();
				MaskWorldGeneratorCache.Scale = SurfaceSettings.MaskScale;
				MaskWorldGeneratorCache.Radius = ToolManager.Radius;
				MaskWorldGeneratorCache.bScaleWithRadius = SurfaceSettings.bScaleWithRadius;
				
				const int32 Size = 2 * ToolManager.Radius / World.VoxelSize;

				if (bNeedToRebuildSeeds || MaskWorldGeneratorCache.Seeds.Num() == 0)
				{
					MaskWorldGeneratorCache.Seeds.Reset();
					for (auto& SeedName : SurfaceSettings.SeedsToRandomize)
					{
						MaskWorldGeneratorCache.Seeds.Add(SeedName, FMath::Rand());
					}
				}
				
				FVoxelFloatTexture FloatTexture;
				UVoxelWorldGeneratorTools::CreateTextureFromWorldGenerator(
					FloatTexture,
					SurfaceSettings.MaskWorldGenerator.GetWorldGenerator(),
					MaskWorldGeneratorCache.Seeds,
					"Value",
					Size,
					Size,
					SurfaceSettings.MaskScale * (SurfaceSettings.bScaleWithRadius ? 100.f / (ToolManager.Radius / World.VoxelSize) : 1.f),
					-Size / 2, -Size / 2,
					World.VoxelSize);
				
				MaskWorldGeneratorCache.Texture = FloatTexture.Texture;
				FVoxelTextureUtilities::CreateOrUpdateUTexture2D(
					FVoxelTextureUtilities::CreateColorTextureFromFloatTexture(MaskWorldGeneratorCache.Texture, EVoxelRGBA::R, true),
					MaskWorldGeneratorCache.RenderTexture);
			}
			
			const_cast<FVoxelToolManager_SurfaceSettings&>(SurfaceSettings).MaskWorldGeneratorDebugTexture = MaskWorldGeneratorCache.RenderTexture;
			
			MaskData.MaskTexture = MaskWorldGeneratorCache.Texture;
			MaskData.MaskRenderTexture = MaskWorldGeneratorCache.RenderTexture;
			MaskData.MaskScaleX = 1.f;
			MaskData.MaskScaleY = SurfaceSettings.MaskRatio;
			MaskData.MaskChannel = EVoxelRGBA::R;
		}
		
		UVoxelSurfaceTools::GetStrengthMaskBasisImpl(GetToolNormal(), GetToolDirection(), MaskData.MaskPlaneX, MaskData.MaskPlaneY);
			
		MaterialInstance.SetVectorParameterValue(STATIC_FNAME("MaskX"), MaskData.MaskPlaneX);
		MaterialInstance.SetVectorParameterValue(STATIC_FNAME("MaskY"), MaskData.MaskPlaneY);

		{
			const FVector TextureMaskScale =
				World.VoxelSize *
				FVector(
					MaskData.MaskScaleX * MaskData.MaskTexture.GetSizeX(),
					MaskData.MaskScaleY * MaskData.MaskTexture.GetSizeY(),
					0);
			MaterialInstance.SetVectorParameterValue(STATIC_FNAME("MaskScale"), TextureMaskScale);
		}
			
		{
			FVector4 MaskChannelVector = FVector4(ForceInit);
			MaskChannelVector[int32(MaskData.MaskChannel)] = 1;
			MaterialInstance.SetVectorParameterValue(STATIC_FNAME("MaskChannel"), FLinearColor(MaskChannelVector));
		}

		MaterialInstance.SetTextureParameterValue(STATIC_FNAME("MaskTexture"), MaskData.MaskRenderTexture);
		MaterialInstance.SetScalarParameterValue(STATIC_FNAME("UseMask"), true);
#else
		FVoxelMessages::ShowVoxelPluginProError("Using masks requires the Pro version of Voxel Plugin");
		return;
#endif
	}

	SetToolRenderingBounds(World, FBox(GetToolPreviewPosition() - ToolManager.Radius, GetToolPreviewPosition() + ToolManager.Radius));

	if (!CanEdit())
	{
		return;
	}

	const float MovementStrengthMultiplier = SurfaceSettings.bMovementAffectsStrength ? GetMouseMovementSize() / 100 : 1;
	const float SculptStrength =
		SurfaceSettings.SculptStrength *
		(SurfaceSettings.bUseMask
			? SurfaceSettings.MaskStrength * FMath::Max(FMath::Abs(MaskData.MaskTexture.GetMin()), FMath::Abs(MaskData.MaskTexture.GetMax()))
			: 1) *
		MovementStrengthMultiplier;
	const float PaintStrength = SurfaceSettings.PaintStrength * MovementStrengthMultiplier;

	constexpr float DistanceDivisor = 4.f;

	const FIntBox BoundsToDoEditsIn = UVoxelBlueprintLibrary::MakeIntBoxFromGlobalPositionAndRadius(&World, GetToolPosition(), ToolManager.Radius);
	const FIntBox BoundsWhereEditsHappen =
		SurfaceSettings.b2DBrush
		? BoundsToDoEditsIn.Extend(FIntVector(0, 0, FMath::CeilToInt(SculptStrength + DistanceDivisor + 2)))
		: BoundsToDoEditsIn;
	
	if (!BoundsToDoEditsIn.IsValid() || !BoundsWhereEditsHappen.IsValid())
	{
		FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
		return;
	}

	// Don't cache the entire column
	const auto BoundsToCache = GetAndDebugBoundsToCache(World, BoundsToDoEditsIn, TickData);

	{
		auto& Data = World.GetData();

		FVoxelWriteScopeLock Lock(Data, BoundsWhereEditsHappen.Union(BoundsToCache), FUNCTION_FNAME);

		if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);
		
		TArray<FSurfaceVoxel> Voxels;
		if (SurfaceSettings.b2DBrush)
		{
			UVoxelSurfaceTools::FindSurfaceVoxels2DImpl<false>(Data, BoundsToDoEditsIn, Voxels);
		}
		else
		{
			UVoxelSurfaceTools::FindSurfaceVoxelsFromDistanceFieldImpl(Data, BoundsToDoEditsIn, SculptStrength + 3, true, Voxels);
		}
		
		TArray<FSurfaceVoxelWithStrength> Strengths;
		{
			Strengths = UVoxelSurfaceTools::AddStrengthToSurfaceVoxels(Voxels);
		
			const FVector ToolVoxelPosition = World.GlobalToLocalFloat(GetToolPosition());

			if (SurfaceSettings.bEnableFalloff)
			{
				const auto ApplyFalloff = [&](auto F)
				{
					if (SurfaceSettings.b2DBrush)
					{
						UVoxelSurfaceTools::ApplyStrengthFunctionImpl<true>(
							Strengths,
							ToolVoxelPosition,
							F);
					}
					else
					{
						UVoxelSurfaceTools::ApplyStrengthFunctionImpl<false>(
							Strengths,
							ToolVoxelPosition,
							F);
					}
				};

				FVoxelToolManagerToolsHelpers::DispatchApplyFalloff(
					SurfaceSettings.FalloffType,
					ToolManager.Radius / World.VoxelSize,
					SurfaceSettings.Falloff,
					ApplyFalloff);
			}

			if (SurfaceSettings.bUseMask)
			{
#if VOXEL_PLUGIN_PRO
				UVoxelSurfaceTools::ApplyStrengthMaskImpl(
					Strengths,
					MaskData.MaskTexture,
					ToolVoxelPosition,
					MaskData.MaskScaleX,
					MaskData.MaskScaleY,
					MaskData.MaskPlaneX,
					MaskData.MaskPlaneY,
					EVoxelSamplerMode::Tile);
#endif
			}
		}

		const float SignedSculptStrength = SculptStrength * (TickData.bAlternativeMode ? 1 : -1);
		const float SignedPaintStrength = PaintStrength * (TickData.bAlternativeMode ? -1 : 1);
		if (SurfaceSettings.bSculpt && SurfaceSettings.bPaint)
		{
			auto SculptStrengths = Strengths;
			UVoxelSurfaceTools::ApplyConstantStrengthImpl(SculptStrengths, SignedSculptStrength);

			TArray<FModifiedVoxelValue> ModifiedVoxels;
			const auto ValueEdits = UVoxelSurfaceTools::CreateValueEditsFromSurfaceVoxels(SculptStrengths);
			if (SurfaceSettings.b2DBrush)
			{
				UVoxelSurfaceTools::EditVoxelValues2DImpl(Data, BoundsWhereEditsHappen, ValueEdits, FVoxelHardnessHandler(World), DistanceDivisor);
			}
			else
			{
				UVoxelSurfaceTools::EditVoxelValuesImpl(Data, BoundsWhereEditsHappen, ValueEdits, FVoxelHardnessHandler(World), DistanceDivisor);
			}
			
			TArray<FVoxelMaterialEdit> Materials;
			Materials.Reserve(ModifiedVoxels.Num());
			for (auto& Voxel : ModifiedVoxels)
			{
				if (Voxel.OldValue > 0 && Voxel.NewValue <= 0)
				{
					Materials.Emplace(FVoxelEditBase(Voxel.Position, SurfaceSettings.PaintStrength), ToolManager.PaintMaterial);
				}
			}
			UVoxelSurfaceTools::EditVoxelMaterialsImpl(Data, BoundsWhereEditsHappen, Materials);
		}
		else if (SurfaceSettings.bSculpt)
		{
			auto SculptStrengths = Strengths;
			UVoxelSurfaceTools::ApplyConstantStrengthImpl(SculptStrengths, SignedSculptStrength);
			
			const auto ValueEdits = UVoxelSurfaceTools::CreateValueEditsFromSurfaceVoxels(SculptStrengths);
			
			if (SurfaceSettings.b2DBrush)
			{
				UVoxelSurfaceTools::EditVoxelValues2DImpl(Data, BoundsWhereEditsHappen, ValueEdits, FVoxelHardnessHandler(World), DistanceDivisor);
			}
			else
			{
				UVoxelSurfaceTools::EditVoxelValuesImpl(Data, BoundsWhereEditsHappen, ValueEdits, FVoxelHardnessHandler(World), DistanceDivisor);
			}
		}
		else if (SurfaceSettings.bPaint)
		{
			// Note: Painting behaves the same with 2D edit on/off
			auto MaterialStrengths = Strengths;
			UVoxelSurfaceTools::ApplyConstantStrengthImpl(MaterialStrengths, SignedPaintStrength);
			const auto MaterialEdits = UVoxelSurfaceTools::CreateMaterialEditsFromSurfaceVoxels(MaterialStrengths, ToolManager.PaintMaterial);
			UVoxelSurfaceTools::EditVoxelMaterialsImpl(Data, BoundsWhereEditsHappen, MaterialEdits);
		}

		if (ToolManager.bDebug)
		{
			UVoxelSurfaceTools::DebugSurfaceVoxels(&World, Strengths, SurfaceSettings.bEnableStride ? 1 : 2 * TickData.DeltaTime);
		}
	}

	SaveFrameOnEndClick(BoundsWhereEditsHappen);
	UpdateWorld(World, BoundsWhereEditsHappen);
}

void FVoxelToolManagerTool_Surface::AddReferencedObjects(FReferenceCollector& Collector)
{
	FVoxelToolManagerTool::AddReferencedObjects(Collector);
	Collector.AddReferencedObject(MaskWorldGeneratorCache.RenderTexture);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelToolManagerTool_Flatten::FVoxelToolManagerTool_Flatten(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, FlattenToolSettings(ToolManager.FlattenSettings))
	, FlattenSettings(ToolManager.FlattenSettings)
{
}

void FVoxelToolManagerTool_Flatten::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();

	auto& MaterialInstance = GetToolMaterialInstance();
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Radius"), ToolManager.Radius);
	MaterialInstance.SetVectorParameterValue(STATIC_FNAME("Position"), GetToolPreviewPosition());
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Falloff"), FlattenSettings.Falloff);
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("EnableFalloff"), FlattenSettings.bEnableFalloff);
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("FalloffType"), int32(FlattenSettings.FalloffType));

	SetToolRenderingBounds(World, FBox(GetToolPreviewPosition() - ToolManager.Radius, GetToolPreviewPosition() + ToolManager.Radius));

	if (!CanEdit())
	{
		return;
	}

	constexpr float DistanceDivisor = 4.f;

	const FIntBox Bounds = UVoxelBlueprintLibrary::MakeIntBoxFromGlobalPositionAndRadius(&World, GetToolPosition(), ToolManager.Radius);
	if (!Bounds.IsValid())
	{
		FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
		return;
	}
	const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

	FVector FlattenPosition;
	FVector FlattenNormal;
	if (FlattenSettings.bUseAverage)
	{
		FVoxelLineTraceParameters Parameters;
		Parameters.CollisionChannel = World.CollisionPresets.GetObjectType();
		Parameters.DrawDebugType = ToolManager.bDebug ? EDrawDebugTrace::ForOneFrame : EDrawDebugTrace::None;

		const float RaysRadius = FMath::Max(ToolManager.Radius, World.VoxelSize);

		TArray<FVoxelProjectionHit> Hits;
		UVoxelProjectionTools::FindProjectionVoxels(
			Hits,
			&World,
			Parameters,
			GetToolPosition() - TickData.RayDirection * ToolManager.Radius,
			TickData.RayDirection,
			RaysRadius,
			EVoxelProjectionShape::Circle,
			100.f,
			WORLD_MAX);

		FlattenPosition = UVoxelProjectionTools::GetHitsAveragePosition(Hits);
		FlattenNormal = UVoxelProjectionTools::GetHitsAverageNormal(Hits);
	}
	else
	{
		FlattenPosition = GetToolPosition();
		FlattenNormal = GetToolNormal();
	}

	if (!GetLastFrameTickData().bClick)
	{
		LastClickFlattenPosition = FlattenPosition;
		LastClickFlattenNormal = FlattenNormal;
	}

	{
		auto& Data = World.GetData();

		FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

		if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);
		
		TArray<FSurfaceVoxel> Voxels;
		UVoxelSurfaceTools::FindSurfaceVoxelsFromDistanceFieldImpl(Data, Bounds, FlattenSettings.Strength + 3, true, Voxels);
		
		const FVector ToolVoxelPosition = World.GlobalToLocalFloat(GetToolPosition());

		TArray<FSurfaceVoxelWithStrength> Strengths = UVoxelSurfaceTools::AddStrengthToSurfaceVoxels(Voxels);

		if (FlattenSettings.bEnableFalloff)
		{
			const auto ApplyFalloff = [&](auto F)
			{
				UVoxelSurfaceTools::ApplyStrengthFunctionImpl<false>(
					Strengths,
					ToolVoxelPosition,
					F);
			};

			FVoxelToolManagerToolsHelpers::DispatchApplyFalloff(
				FlattenSettings.FalloffType,
				ToolManager.Radius / World.VoxelSize,
				FlattenSettings.Falloff,
				ApplyFalloff);
		}

		UVoxelSurfaceTools::ApplyConstantStrengthImpl(Strengths, FlattenSettings.Strength);

		const FVector PlanePosition = FlattenSettings.bFreezeOnClick ? LastClickFlattenPosition : FlattenPosition;
		const FVector PlaneNormal = FlattenSettings.bFreezeOnClick ? LastClickFlattenNormal : FlattenNormal;

		const FPlane Plane(
			World.GlobalToLocalFloat(PlanePosition),
			World.GetActorTransform().InverseTransformVector(PlaneNormal).GetSafeNormal());
		
		UVoxelSurfaceTools::ApplyFlattenImpl(
			Strengths,
			Plane,
			TickData.bAlternativeMode ? EVoxelSDFMergeMode::Intersection : EVoxelSDFMergeMode::Union,
			true);

		const auto ValueEdits = UVoxelSurfaceTools::CreateValueEditsFromSurfaceVoxels(Strengths);
		UVoxelSurfaceTools::EditVoxelValuesImpl(Data, Bounds, ValueEdits, FVoxelHardnessHandler(World), DistanceDivisor);

		if (ToolManager.bDebug)
		{
			UVoxelSurfaceTools::DebugSurfaceVoxels(&World, Strengths, 2 * TickData.DeltaTime);

			DrawDebugSolidPlane(
				TickData.World,
				FPlane(PlanePosition, PlaneNormal),
				PlanePosition,
				1000000,
				FColor::Red,
				false,
				1.5f * TickData.DeltaTime);
		}
	}

	SaveFrameOnEndClick(Bounds);
	UpdateWorld(World, Bounds);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelToolManagerTool_Trim::FVoxelToolManagerTool_Trim(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, TrimToolSettings(ToolManager.TrimSettings))
	, TrimSettings(ToolManager.TrimSettings)
{
}

void FVoxelToolManagerTool_Trim::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();
	
	FVoxelLineTraceParameters Parameters;
	Parameters.CollisionChannel = World.CollisionPresets.GetObjectType();
	Parameters.DrawDebugType = ToolManager.bDebug ? EDrawDebugTrace::ForOneFrame : EDrawDebugTrace::None;

	TArray<FVoxelProjectionHit> Hits;
	const float RaysRadius = FMath::Max(ToolManager.Radius * (1 - TrimSettings.Roughness), World.VoxelSize);
	UVoxelProjectionTools::FindProjectionVoxels(
		Hits,
		&World,
		Parameters,
		GetToolPosition() - TickData.RayDirection * ToolManager.Radius,
		TickData.RayDirection,
		RaysRadius,
		EVoxelProjectionShape::Circle,
		100.f,
		WORLD_MAX);

	const FVector Position = UVoxelProjectionTools::GetHitsAveragePosition(Hits);

	SetToolRenderingBounds(World, FBox(Position - ToolManager.Radius, Position + ToolManager.Radius));

	auto& MaterialInstance = GetToolMaterialInstance();
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Radius"), ToolManager.Radius);
	MaterialInstance.SetVectorParameterValue(STATIC_FNAME("Position"), Position);
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Falloff"), TrimSettings.Falloff);
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Roughness"), TrimSettings.Roughness);

	if (!CanEdit())
	{
		return;
	}

	const FVector Normal = UVoxelProjectionTools::GetHitsAverageNormal(Hits);
	
	const FVector VoxelPosition = World.GlobalToLocalFloat(Position);
	const float VoxelRadius = ToolManager.Radius / World.VoxelSize;

	const FIntBox Bounds = UVoxelSphereTools::GetSphereBounds(VoxelPosition, VoxelRadius);
	if (!Bounds.IsValid())
	{
		FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
		return;
	}
	const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

	{
		auto& Data = World.GetData();

		FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

		if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);

		UVoxelSphereTools::TrimSphereImpl(
			Data,
			VoxelPosition,
			Normal,
			VoxelRadius * (1 - TrimSettings.Falloff),
			VoxelRadius * TrimSettings.Falloff,
			!TickData.bAlternativeMode);
	}

	SaveFrameOnEndClick(Bounds);
	UpdateWorld(World, Bounds);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelToolManagerTool_Level::FVoxelToolManagerTool_Level(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, LevelToolSettings(ToolManager.LevelSettings))
	, LevelSettings(ToolManager.LevelSettings)
{
}

void FVoxelToolManagerTool_Level::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();

	const FVector ToolOffset = FVector(0.f, 0.f, TickData.bAlternativeMode ? -LevelSettings.Offset : LevelSettings.Offset);
	
	const float ScaleXY = ToolManager.Radius / 50.f;
	const float ScaleZ = LevelSettings.Height / 100.f;
	SetToolMeshTransform(TickData.World, FTransform(
		FQuat::Identity,
		GetToolPreviewPosition() + ToolOffset + FVector(0, 0, (TickData.bAlternativeMode ? LevelSettings.Height : -LevelSettings.Height) / 2),
		FVector(ScaleXY, ScaleXY, ScaleZ)));

	if (!CanEdit())
	{
		return;
	}

	const FVector VoxelPosition = World.GlobalToLocalFloat(GetToolPosition() + ToolOffset);
	const float VoxelRadius = ToolManager.Radius / World.VoxelSize;
	const float VoxelHeight = LevelSettings.Height / World.VoxelSize;

	const FIntBox Bounds = UVoxelDataTools::GetLevelToolBounds(VoxelPosition, VoxelRadius, VoxelHeight, !TickData.bAlternativeMode);
	if (!Bounds.IsValid())
	{
		FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
		return;
	}
	const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

	{
		auto& Data = World.GetData();

		FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

		if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);

		UVoxelDataTools::LevelImpl(
			Data,
			VoxelPosition,
			VoxelRadius,
			LevelSettings.Falloff,
			VoxelHeight,
			!TickData.bAlternativeMode);
	}

	SaveFrameOnEndClick(Bounds);
	UpdateWorld(World, Bounds);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelToolManagerTool_Smooth::FVoxelToolManagerTool_Smooth(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, SmoothToolSettings(ToolManager.SmoothSettings))
	, SmoothSettings(ToolManager.SmoothSettings)
{
}

void FVoxelToolManagerTool_Smooth::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();
	
	SetToolRenderingBounds(World, FBox(GetToolPreviewPosition() - ToolManager.Radius, GetToolPreviewPosition() + ToolManager.Radius));

	auto& MaterialInstance = GetToolMaterialInstance();
	MaterialInstance.SetScalarParameterValue(STATIC_FNAME("Radius"), ToolManager.Radius);
	MaterialInstance.SetVectorParameterValue(STATIC_FNAME("Position"), GetToolPreviewPosition());

	if (!CanEdit())
	{
		return;
	}

	const FVector VoxelPosition = World.GlobalToLocalFloat(GetToolPosition());
	const float VoxelRadius = ToolManager.Radius / World.VoxelSize;

	const FIntBox Bounds = UVoxelSphereTools::GetSphereBounds(VoxelPosition, VoxelRadius);
	if (!Bounds.IsValid())
	{
		FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
		return;
	}
	const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

	{
		auto& Data = World.GetData();

		FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

		if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);

		if (TickData.bAlternativeMode)
		{
			UVoxelSphereTools::SharpenSphereImpl(
				Data,
				VoxelPosition,
				VoxelRadius,
				SmoothSettings.Strength);
		}
		else
		{
			UVoxelSphereTools::SmoothSphereImpl(
				Data,
				VoxelPosition,
				VoxelRadius,
				SmoothSettings.Strength);
		}
	}

	SaveFrameOnEndClick(Bounds);
	UpdateWorld(World, Bounds);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelToolManagerTool_Sphere::FVoxelToolManagerTool_Sphere(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, SphereToolSettings(ToolManager.SphereSettings))
	, SphereSettings(ToolManager.SphereSettings)
{
}

void FVoxelToolManagerTool_Sphere::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();

	if (!SphereSettings.SphereMesh)
	{
		FVoxelMessages::Error("VoxelToolManager: Sphere Tool: Invalid SphereMesh!");
	}

	const float Scale = ToolManager.Radius / 50;
	SetToolMeshTransform(TickData.World, FTransform(FQuat::Identity, GetToolPreviewPosition(), FVector(Scale)));

	if (!CanEdit())
	{
		return;
	}

	const FVector VoxelPosition = World.GlobalToLocalFloat(GetToolPosition());
	const float VoxelRadius = ToolManager.Radius / World.VoxelSize;

	const FIntBox Bounds = UVoxelSphereTools::GetSphereBounds(VoxelPosition, VoxelRadius);
	if (!Bounds.IsValid())
	{
		FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
		return;
	}
	const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

	{
		auto& Data = World.GetData();

		FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

		if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);

		if (TickData.bAlternativeMode)
		{
			UVoxelSphereTools::RemoveSphereImpl(
				Data,
				VoxelPosition,
				VoxelRadius);
		}
		else
		{
			if (SphereSettings.bPaint)
			{
				TArray<FModifiedVoxelValue> ModifiedVoxels;
				UVoxelSphereTools::AddSphereImpl(
					Data,
					VoxelPosition,
					VoxelRadius,
					ModifiedVoxels);

				TArray<FVoxelMaterialEdit> Materials;
				Materials.Reserve(ModifiedVoxels.Num());
				for (auto& Voxel : ModifiedVoxels)
				{
					if (Voxel.OldValue > 0 && Voxel.NewValue <= 0)
					{
						Materials.Emplace(FVoxelEditBase(Voxel.Position, SphereSettings.PaintStrength), ToolManager.PaintMaterial);
					}
				}
				UVoxelSurfaceTools::EditVoxelMaterialsImpl(Data, Bounds, Materials);

			}
			else
			{
				UVoxelSphereTools::AddSphereImpl(
					Data,
					VoxelPosition,
					VoxelRadius);
			}
		}
	}

	SaveFrameOnEndClick(Bounds);
	UpdateWorld(World, Bounds);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#if VOXEL_PLUGIN_PRO
FVoxelToolManagerTool_Mesh::FVoxelToolManagerTool_Mesh(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, MeshToolSettings(ToolManager.MeshSettings))
	, MeshSettings(ToolManager.MeshSettings)
{
}

FVoxelToolManagerTool_Mesh::~FVoxelToolManagerTool_Mesh()
{
}

void FVoxelToolManagerTool_Mesh::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();

	if (!MeshSettings.Mesh)
	{
		return;
	}

	if (MeshData.IsValid() && MeshData->StaticMesh != MeshSettings.Mesh)
	{
		MeshData.Reset();
		AssetData.Reset();
	}
	if (!MeshData.IsValid())
	{
		auto NewMeshData = MakeUnique<FMeshData>();
		NewMeshData->StaticMesh = MeshSettings.Mesh;
		UVoxelMeshImporterLibrary::CreateMeshDataFromStaticMesh(MeshSettings.Mesh, NewMeshData->Data);

		NewMeshData->Bounds = FBox(ForceInit);
		for (auto& Vertex : NewMeshData->Data.Vertices)
		{
			NewMeshData->Bounds += Vertex;
		}

		MeshData = MoveTemp(NewMeshData);

		if (MeshData->Data.Vertices.Num() == 0)
		{
			FVoxelMessages::Error("ToolManager: Mesh Tool: Failed to extract mesh data!");
		}
	}
	check(MeshData.IsValid());

	if (MeshData->Data.Vertices.Num() == 0)
	{
		return;
	}

	const FVector Scale =
		MeshSettings.bUseMeshScale
		? MeshSettings.Scale
		: 2 * ToolManager.Radius * MeshSettings.Scale / MeshData->Bounds.GetSize().GetMax(); // 2x: we want the diameter, not the radius

	const auto GetRotationMatrix = [&]()
	{
		const FVector X = MeshSettings.bAlignToMovement ? GetToolDirection() : FVector::ForwardVector;
		const FVector Z = MeshSettings.bAlignToNormal ? GetToolNormal() : FVector::UpVector;
		const FVector Y = (Z ^ X).GetSafeNormal();
		return FMatrix(Y ^ Z, Y, Z, FVector(0));
	};

	// Matrix and Transform multiplications are left to right!

	const FMatrix ScaleMatrix = FScaleMatrix(Scale);
	const FVector PositionOffset = MeshSettings.PositionOffset * ScaleMatrix.TransformVector(MeshData->Bounds.GetSize());

	const FTransform TransformNoTranslation = FTransform(
		ScaleMatrix *
		FTranslationMatrix(PositionOffset) *
		FRotationMatrix(MeshSettings.RotationOffset) *
		GetRotationMatrix());
	const FTransform TransformWithTranslation = TransformNoTranslation * FTransform(GetToolPreviewPosition());

	SetToolMeshTransform(TickData.World, TransformWithTranslation);

	if (!CanEdit())
	{
		return;
	}

	const bool bSetSingleIndex = MeshSettings.bPaintIndex && World.MaterialConfig == EVoxelMaterialConfig::SingleIndex;
	const bool bSetDoubleIndex = MeshSettings.bPaintIndex && World.MaterialConfig == EVoxelMaterialConfig::DoubleIndex;

	if (MeshSettings.bSmoothImport || MeshSettings.bProgressiveStamp)
	{
		const float Smoothness = MeshSettings.Smoothness * Scale.GetMax() * MeshData->Bounds.GetSize().GetMax() / World.VoxelSize;
		const float MaxDistance =
			MeshSettings.bProgressiveStamp
			? 1e9
			: Smoothness + 4;

		if (DistanceFieldData.IsValid() && (
			!TransformNoTranslation.Equals(DistanceFieldData->Transform) ||
			MaxDistance != DistanceFieldData->MaxDistance))
		{
			DistanceFieldData.Reset();
		}
		if (!DistanceFieldData.IsValid())
		{
			auto NewDistanceFieldData = MakeUnique<FDistanceFieldData>();

			NewDistanceFieldData->Transform = TransformNoTranslation;
			NewDistanceFieldData->MaxDistance = MaxDistance;

			int32 NumLeaks;
			UVoxelMeshImporterLibrary::ConvertMeshToDistanceField(
				MeshData->Data,
				TransformNoTranslation,
				World.VoxelSize,
				MaxDistance,
				MeshSettings.bProgressiveStamp ? 0 : MaxDistance,
				NewDistanceFieldData->Data,
				NewDistanceFieldData->Size,
				NewDistanceFieldData->PositionOffset,
				NumLeaks);

			if (ToolManager.bDebug)
			{
				GEngine->AddOnScreenDebugMessage(
					OBJECT_LINE_ID(),
					1.5f * TickData.DeltaTime,
					FColor::Yellow,
					"Converting mesh to distance field");
			}

			DistanceFieldData = MoveTemp(NewDistanceFieldData);
		}
		check(DistanceFieldData.IsValid());

		const FIntVector VoxelPosition = World.GlobalToLocal(GetToolPosition()) + DistanceFieldData->PositionOffset;
		const FIntBox Bounds = FIntBox(VoxelPosition, VoxelPosition + DistanceFieldData->Size);
		if (!Bounds.IsValid())
		{
			FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
			return;
		}
		const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

		{
			auto& Data = World.GetData();

			FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

			if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);

			const FIntVector Size = DistanceFieldData->Size;
			const auto& DistanceField = DistanceFieldData->Data;
			
			UVoxelDataTools::MergeDistanceFieldImpl(
				Data,
				Bounds,
				[&](int32 X, int32 Y, int32 Z)
				{
					X -= Bounds.Min.X;
					Y -= Bounds.Min.Y;
					Z -= Bounds.Min.Z;

					checkVoxelSlow(0 < X && X < Size.X);
					checkVoxelSlow(0 < Y && Y < Size.Y);
					checkVoxelSlow(0 < Z && Z < Size.Z);

					return DistanceField[X + Size.X * Y + Size.X * Size.Y * Z];
				},
				[&](float A, float B)
				{
					if (MeshSettings.bProgressiveStamp)
					{
						if (TickData.bAlternativeMode)
						{
							return FMath::Lerp(A, FMath::Max(A, -B), MeshSettings.Speed);
						}
						else
						{
							return FMath::Lerp(A, FMath::Min(A, B), MeshSettings.Speed);
						}
					}
					else
					{
						ensureVoxelSlowNoSideEffects(MeshSettings.bSmoothImport);
						if (TickData.bAlternativeMode)
						{
							return FVoxelUtilities::SmoothSubtraction(B, A, Smoothness);
						}
						else
						{
							return FVoxelUtilities::SmoothUnion(B, A, Smoothness);
						}
					}
				},
				MaxDistance,
				true);
		}

		SaveFrameOnEndClick(Bounds);
		UpdateWorld(World, Bounds);
	}
	else
	{
		if (AssetData.IsValid() && !TransformNoTranslation.Equals(AssetData->Transform))
		{
			AssetData.Reset();
		}
		if (!AssetData.IsValid())
		{
			auto NewAssetData = MakeUnique<FAssetData>();

			NewAssetData->Transform = TransformNoTranslation;

			if (!RenderTargetCache) RenderTargetCache = MakeUnique<FVoxelMeshImporterRenderTargetCache>();

			FVoxelMeshImporterSettings MeshImporterSettings;
			MeshImporterSettings.VoxelSize = World.VoxelSize;

			MeshImporterSettings.bPaintColors = MeshSettings.bPaint && MeshSettings.bPaintColors && MeshSettings.ColorsMaterial;
			MeshImporterSettings.ColorsMaterial = MeshSettings.ColorsMaterial;

			MeshImporterSettings.bPaintUVs = MeshSettings.bPaint && MeshSettings.bPaintUVs && MeshSettings.UVsMaterial;
			MeshImporterSettings.UVsMaterial = MeshSettings.UVsMaterial;

			MeshImporterSettings.RenderTargetSize = MeshSettings.RenderTargetSize;

			MeshImporterSettings.bSetSingleIndex = bSetSingleIndex;
			MeshImporterSettings.bSetDoubleIndex = bSetDoubleIndex;

			MeshImporterSettings.SingleIndex = MeshSettings.Index;
			MeshImporterSettings.DoubleIndex = MeshSettings.Index;

			int32 NumLeaks;
			UVoxelMeshImporterLibrary::ConvertMeshToVoxels(
				TickData.World,
				MeshData->Data,
				TransformNoTranslation,
				MeshImporterSettings,
				*RenderTargetCache,
				NewAssetData->Data,
				NewAssetData->PositionOffset,
				NumLeaks);

			const_cast<FVoxelToolManager_MeshSettings&>(MeshSettings).ColorsRenderTarget = RenderTargetCache->ColorsRenderTarget;
			const_cast<FVoxelToolManager_MeshSettings&>(MeshSettings).UVsRenderTarget = RenderTargetCache->UVsRenderTarget;

			if (ToolManager.bDebug)
			{
				GEngine->AddOnScreenDebugMessage(
					OBJECT_LINE_ID(),
					1.5f * TickData.DeltaTime,
					FColor::Yellow,
					"Converting mesh to voxels");
			}

			AssetData = MoveTemp(NewAssetData);
		}
		check(AssetData.IsValid());

		const FVector VoxelPosition = World.GlobalToLocalFloat(GetToolPosition()) + FVector(AssetData->PositionOffset);
		const FIntBox Bounds = FIntBox(VoxelPosition, VoxelPosition + FVector(AssetData->Data.GetSize()));
		if (!Bounds.IsValid())
		{
			FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
			return;
		}
		const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

		{
			auto& Data = World.GetData();

			FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

			if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);

			uint32 MaterialMask = EVoxelMaterialMask::None;
			if (MeshSettings.bPaintColors)
			{
				MaterialMask |= EVoxelMaterialMask::Color;
			}
			if (MeshSettings.bPaintUVs)
			{
				MaterialMask |= EVoxelMaterialMask::UV;
			}
			if (bSetSingleIndex)
			{
				MaterialMask |= EVoxelMaterialMask::SingleIndex_Index;
			}
			if (bSetDoubleIndex)
			{
				MaterialMask |= EVoxelMaterialMask::DoubleIndex_IndexA;
				MaterialMask |= EVoxelMaterialMask::DoubleIndex_IndexB;
				MaterialMask |= EVoxelMaterialMask::DoubleIndex_Blend;
			}

			if (TickData.bAlternativeMode)
			{
				if (MeshSettings.bSculpt)
				{
					if (!AssetData->InvertedData)
					{
						AssetData->InvertedData = MakeUnique<FVoxelDataAssetData>(nullptr);
						UVoxelAssetTools::InvertDataAssetImpl(AssetData->Data, *AssetData->InvertedData);
					}
					UVoxelAssetTools::ImportDataAssetImpl(
						Data,
						Bounds,
						VoxelPosition,
						*AssetData->InvertedData,
						true,
						EVoxelAssetMergeMode::InnerValues,
						MaterialMask);
				}
			}
			else
			{
				if (MeshSettings.bSculpt && MeshSettings.bPaint)
				{
					UVoxelAssetTools::ImportDataAssetImpl(
						Data,
						Bounds,
						VoxelPosition,
						AssetData->Data,
						false,
						EVoxelAssetMergeMode::InnerValuesAndInnerMaterials,
						MaterialMask);
				}
				else if (MeshSettings.bSculpt)
				{
					UVoxelAssetTools::ImportDataAssetImpl(
						Data,
						Bounds,
						VoxelPosition,
						AssetData->Data,
						false,
						EVoxelAssetMergeMode::InnerValues,
						MaterialMask);
				}
				else if (MeshSettings.bPaint)
				{
					UVoxelAssetTools::ImportDataAssetImpl(
						Data,
						Bounds,
						VoxelPosition,
						AssetData->Data,
						false,
						EVoxelAssetMergeMode::InnerMaterials,
						MaterialMask);
				}
			}
		}

		SaveFrameOnEndClick(Bounds);
		UpdateWorld(World, Bounds);
	}
}

void FVoxelToolManagerTool_Mesh::AddReferencedObjects(FReferenceCollector& Collector)
{
	FVoxelToolManagerTool::AddReferencedObjects(Collector);

	if (RenderTargetCache.IsValid())
	{
		Collector.AddReferencedObject(RenderTargetCache->ColorsRenderTarget);
		Collector.AddReferencedObject(RenderTargetCache->UVsRenderTarget);
		Collector.AddReferencedObject(RenderTargetCache->LastRenderedColorsMaterial);
		Collector.AddReferencedObject(RenderTargetCache->LastRenderedUVsMaterial);
	}
}
#endif

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelToolManagerTool_Revert::FVoxelToolManagerTool_Revert(const UVoxelToolManager& ToolManager)
	: FVoxelToolManagerTool(ToolManager, RevertToolSettings(ToolManager.RevertToGeneratorSettings))
	, RevertSettings(ToolManager.RevertToGeneratorSettings)
{
}

void FVoxelToolManagerTool_Revert::Tick(AVoxelWorld& World, const FVoxelToolManagerTickData& TickData)
{
	VOXEL_FUNCTION_COUNTER();

	const_cast<FVoxelToolManager_RevertSettings&>(RevertSettings).CurrentHistoryPosition = World.GetData().GetHistoryPosition();
	const_cast<FVoxelToolManager_RevertSettings&>(RevertSettings).HistoryPosition = FMath::Clamp(RevertSettings.HistoryPosition, 0, RevertSettings.CurrentHistoryPosition);
	
	if (!RevertSettings.SphereMesh)
	{
		FVoxelMessages::Error("VoxelToolManager: Revert Tool: Invalid SphereMesh!");
	}

	const float Scale = ToolManager.Radius / 50;
	SetToolMeshTransform(TickData.World, FTransform(FQuat::Identity, GetToolPreviewPosition(), FVector(Scale)));

	if (!CanEdit())
	{
		return;
	}

	const FVector VoxelPosition = World.GlobalToLocalFloat(GetToolPosition());
	const float VoxelRadius = ToolManager.Radius / World.VoxelSize;

	const FIntBox Bounds = UVoxelSphereTools::GetSphereBounds(VoxelPosition, VoxelRadius);
	if (!Bounds.IsValid())
	{
		FVoxelMessages::Error("Invalid tool bounds!", &ToolManager);
		return;
	}
	const auto BoundsToCache = GetAndDebugBoundsToCache(World, Bounds, TickData);

	{
		auto& Data = World.GetData();

		FVoxelWriteScopeLock Lock(Data, BoundsToCache, FUNCTION_FNAME);

		if (ToolManager.bCacheData) Data.CacheBounds<FVoxelValue>(BoundsToCache);

		if (TickData.bAlternativeMode)
		{
			UVoxelSphereTools::RevertSphereToGeneratorImpl(
				Data,
				VoxelPosition,
				VoxelRadius,
				RevertSettings.bRevertValues,
				RevertSettings.bRevertMaterials);
		}
		else
		{
			UVoxelSphereTools::RevertSphereImpl(
				Data,
				VoxelPosition,
				VoxelRadius,
				RevertSettings.HistoryPosition,
				RevertSettings.bRevertValues,
				RevertSettings.bRevertMaterials);
		}
	}

	SaveFrameOnEndClick(Bounds);
	UpdateWorld(World, Bounds);
}