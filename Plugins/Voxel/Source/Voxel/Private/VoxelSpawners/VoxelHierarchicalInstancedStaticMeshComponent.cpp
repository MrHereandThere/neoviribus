// Copyright 2020 Phyronnaz

#if VOXEL_PLUGIN_PRO
#include "VoxelSpawners/VoxelHierarchicalInstancedStaticMeshComponent.h"
#include "VoxelSpawners/VoxelHISMBuildTask.h"
#include "VoxelDebug/VoxelDebugManager.h"
#include "VoxelData/VoxelDataUtilities.h"
#include "VoxelData/VoxelDataAccelerator.h"
#include "VoxelGlobals.h"
#include "IVoxelPool.h"

#include "Async/Async.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "Engine/Private/InstancedStaticMesh.h"

DECLARE_DWORD_ACCUMULATOR_STAT(TEXT("Voxel HISM Num Instances"), STAT_VoxelHISMComponent_NumInstances, STATGROUP_VoxelCounters);
DECLARE_DWORD_ACCUMULATOR_STAT(TEXT("Voxel HISM Num Physics Bodies"), STAT_VoxelHISMComponent_NumPhysicsBodies, STATGROUP_VoxelCounters);
DECLARE_DWORD_COUNTER_STAT(TEXT("Voxel HISM Num Floating Mesh Checked"), STAT_VoxelHISMComponent_NumFloatingMeshChecked, STATGROUP_VoxelCounters);
DEFINE_VOXEL_MEMORY_STAT(STAT_VoxelHISMMemory);

static TAutoConsoleVariable<int32> CVarShowHISMCollisions(
	TEXT("voxel.spawners.ShowCollisions"),
	0,
	TEXT("If true, will show a debug point on HISM instances with collisions"),
	ECVF_Default);

static const FMatrix EmptyMatrix = FTransform(FQuat::Identity, FVector::ZeroVector, FVector::ZeroVector).ToMatrixWithScale();

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

UVoxelHierarchicalInstancedStaticMeshComponent::UVoxelHierarchicalInstancedStaticMeshComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
#if WITH_EDITOR
	PrimaryComponentTick.bCanEverTick = true;
#endif

	Voxel_UpdateAllocatedMemory();
}

UVoxelHierarchicalInstancedStaticMeshComponent::~UVoxelHierarchicalInstancedStaticMeshComponent()
{
	DEC_VOXEL_MEMORY_STAT_BY(STAT_VoxelHISMMemory, Voxel_AllocatedMemory);
	DEC_DWORD_STAT_BY(STAT_VoxelHISMComponent_NumInstances, Voxel_UnbuiltMatrices.Num());
	ensure(Voxel_InstanceBodies.Num() == 0);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_Init(
	TVoxelWeakPtr<IVoxelPool> Pool, 
	TVoxelWeakPtr<FVoxelInstancedMeshManager> InstancedMeshManager, 
	float VoxelSize)
{
	Voxel_Pool = Pool;
	Voxel_InstancedMeshManager = InstancedMeshManager;
	Voxel_VoxelSize = VoxelSize;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

TVoxelWeakPtr<FVoxelInstancesSection> UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_AppendTransforms(const TArray<FVoxelSpawnerMatrix>& InTransforms, const FIntBox& InBounds)
{
	VOXEL_FUNCTION_COUNTER();

	if (!ensure(GetStaticMesh())) return nullptr;
	if (!ensure(InTransforms.Num() > 0)) return nullptr;
	
	INC_DWORD_STAT_BY(STAT_VoxelHISMComponent_NumInstances, InTransforms.Num());

	const int32 FirstInstanceIndex = Voxel_UnbuiltMatrices.Num();

	const auto NewSection = MakeVoxelShared<FVoxelInstancesSection>();
	NewSection->StartIndex = FirstInstanceIndex;
	NewSection->Num = InTransforms.Num();
	Voxel_Sections.Emplace(NewSection);

	{
		VOXEL_SCOPE_COUNTER("Append");
		Voxel_UnbuiltMatrices.Append(InTransforms);
	}
	
	Voxel_PendingNewInstancesBounds.Add(InBounds);

	Voxel_ScheduleBuildTree();
	Voxel_UpdateAllocatedMemory();

	return NewSection;
}

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_RemoveSection(const TVoxelWeakPtr<FVoxelInstancesSection>& Section)
{
	VOXEL_FUNCTION_COUNTER();

	const auto PinnedSection = Section.Pin();
	if (!ensure(PinnedSection.IsValid())) return;

	const int32 StartIndex = PinnedSection->StartIndex;
	const int32 Num = PinnedSection->Num;
	
	if (!ensure(Voxel_UnbuiltMatrices.IsValidIndex(StartIndex)) ||
		!ensure(StartIndex + Num <= Voxel_UnbuiltMatrices.Num())) return;

	if (!ensure(Voxel_Sections.Remove(PinnedSection) == 1)) return;

	ensure(PinnedSection.IsUnique());
	
	const auto Fix = [&](int32& Index)
	{
		ensure(Voxel_UnbuiltMatrices.IsValidIndex(Index));
		if (Index >= StartIndex)
		{
			if (Index < StartIndex + Num)
			{
				Index = -1;
			}
			else
			{
				Index -= Num;
			}
		}
	};

	{
		VOXEL_SCOPE_COUNTER("Fix Existing Sections");
		for (auto& ExistingSection : Voxel_Sections)
		{
			Fix(ExistingSection->StartIndex);
		}
	}
	
	{
		VOXEL_SCOPE_COUNTER("Fix UnbuiltInstancesToClear");
		for (auto& Index : Voxel_UnbuiltInstancesToClear)
		{
			Fix(Index);
		}
		Voxel_UnbuiltInstancesToClear.RemoveSwap(-1);
	}

	{
		VOXEL_SCOPE_COUNTER("Fix bodies");
		for (auto& It : Voxel_InstanceBodies)
		{
			for (auto& Body : It.Value)
			{
				Fix(Body->InstanceBodyIndex);
				if (Body->InstanceBodyIndex == -1)
				{
					DEC_DWORD_STAT(STAT_VoxelHISMComponent_NumPhysicsBodies);
					Body->TermBody();
					delete Body;
					Body = nullptr;
				}
			}
			It.Value.RemoveSwap(nullptr);
		}
	}

	{
		VOXEL_SCOPE_COUNTER("RemoveAt");
		Voxel_UnbuiltMatrices.RemoveAt(StartIndex, Num);
	}
	DEC_DWORD_STAT_BY(STAT_VoxelHISMComponent_NumInstances, Num);

	if (Voxel_UnbuiltMatrices.Num() == 0)
	{
		ensure(Voxel_UnbuiltInstancesToClear.Num() == 0);
		// Having 0 instances creates a whole bunch of issues, so add one
		Voxel_UnbuiltMatrices.Add(FVoxelSpawnerMatrix(EmptyMatrix));
	}

	// Generate a dummy ID to order deletions with tasks
	const uint64 UniqueIdForOrder = FVoxelHISMBuildTask::GetUniqueId();
	Voxel_Mappings.DeletionsStack.Add({ UniqueIdForOrder, StartIndex, Num });

#if VOXEL_DEBUG
	{
		VOXEL_SCOPE_COUNTER("Debug");
		for (int32 UnbuiltIndex = 0; UnbuiltIndex < Voxel_UnbuiltMatrices.Num(); UnbuiltIndex++)
		{
			const int32 BuiltIndex = Voxel_Mappings.GetBuiltIndex(UnbuiltIndex);
			if (BuiltIndex == -1) continue;
			ensure(Voxel_UnbuiltMatrices[UnbuiltIndex] == Voxel_BuiltMatrices[BuiltIndex]);
		}
		for (int32 BuildIndex = 0; BuildIndex < Voxel_BuiltMatrices.Num(); BuildIndex++)
		{
			const int32 UnbuiltIndex = Voxel_Mappings.GetUnbuiltIndex(BuildIndex);
			if (UnbuiltIndex == -1) continue;
			ensure(Voxel_UnbuiltMatrices[UnbuiltIndex] == Voxel_BuiltMatrices[BuildIndex]);
		}
	}
#endif

	Voxel_ScheduleBuildTree();
	Voxel_UpdateAllocatedMemory();
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_StartBuildTree()
{
	VOXEL_FUNCTION_COUNTER();

	ensure((Voxel_TaskUniqueId != 0) == Voxel_TaskCancelCounterPtr.IsValid());
	
	if (!ensure(GetStaticMesh())) return;
	if (!ensure(Voxel_UnbuiltMatrices.Num() > 0)) return;

	auto Pool = Voxel_Pool.Pin();
	if (!ensure(Pool.IsValid())) return;

	if (Voxel_TaskCancelCounterPtr.IsValid())
	{
		Voxel_TaskCancelCounterPtr->Increment();
		Voxel_TaskCancelCounterPtr.Reset();
	}
	
	auto* Task = new FVoxelHISMBuildTask(this, Voxel_UnbuiltMatrices);
	Voxel_TaskUniqueId = Task->UniqueId;
	Voxel_TaskCancelCounterPtr = Task->CancelCounter;
	Pool->QueueTask(EVoxelTaskType::HISMBuild, Task);

	ensure(!Voxel_TaskIdToNewInstancesBounds.Contains(Voxel_TaskUniqueId));
	// Not true when removing instances ensure(Voxel_PendingNewInstancesBounds.Num() > 0);
	Voxel_TaskIdToNewInstancesBounds.Add(Voxel_TaskUniqueId, MoveTemp(Voxel_PendingNewInstancesBounds));

	Voxel_UpdateAllocatedMemory();
}

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_FinishBuilding(FVoxelHISMBuiltData& BuiltData)
{
	VOXEL_FUNCTION_COUNTER();

	ensure((Voxel_TaskUniqueId != 0) == Voxel_TaskCancelCounterPtr.IsValid());

	if (Voxel_TaskUniqueId == 0)
	{
		// Task is too late, newer one already completed
		ensure(!Voxel_TaskIdToNewInstancesBounds.Contains(BuiltData.UniqueId));
		ensure(Voxel_UnbuiltInstancesToClear.Num() == 0);
		return;
	}

	ensure(Voxel_TaskUniqueId >= BuiltData.UniqueId); // Else the task time traveled
	const bool bIsLatestTask = BuiltData.UniqueId == Voxel_TaskUniqueId;

	for (int32 UnbuiltIndex : Voxel_UnbuiltInstancesToClear)
	{
		if (!BuiltData.InstancesToBuiltInstances.IsValidIndex(UnbuiltIndex))
		{
			ensure(!bIsLatestTask);
			continue;
		}
		const int32 BuiltIndex = BuiltData.InstancesToBuiltInstances[UnbuiltIndex];
		BuiltData.InstanceBuffer->SetInstance(BuiltIndex, EmptyMatrix, 0);
		BuiltData.BuiltInstancesMatrices[BuiltIndex] = FVoxelSpawnerMatrix(EmptyMatrix);
	}
	
	if (bIsLatestTask)
	{
		Voxel_TaskUniqueId = 0;
		Voxel_TaskCancelCounterPtr.Reset();
		Voxel_UnbuiltInstancesToClear.Reset();
	}
	else
	{
		// Do not reset the task id nor cancel it as this is not the right one yet
	}

	const int32 NumInstances = BuiltData.InstanceBuffer->GetNumInstances();
	check(NumInstances > 0);
	check(NumInstances == BuiltData.BuiltInstancesMatrices.Num());
	check(NumInstances == BuiltData.InstancesToBuiltInstances.Num());
	check(NumInstances == BuiltData.BuiltInstancesToInstances.Num());
	// Not true with timer delays ensure(!bIsLatestTask || NumInstances == Voxel_UnbuiltMatrices.Num());

	Voxel_BuiltMatrices = MoveTemp(BuiltData.BuiltInstancesMatrices);

	Voxel_Mappings.InstancesToBuiltInstances = MoveTemp(BuiltData.InstancesToBuiltInstances);
	Voxel_Mappings.BuiltInstancesToInstances = MoveTemp(BuiltData.BuiltInstancesToInstances);

	// Remove any old deletions
	// < safe as we are generating a dummy id when deleting
	Voxel_Mappings.DeletionsStack.RemoveAll([&](auto& Deletion) { return Deletion.TaskUniqueId < BuiltData.UniqueId; });
	
	constexpr bool bRequireCPUAccess = true;
	if (!PerInstanceRenderData.IsValid() || PerInstanceRenderData->InstanceBuffer.RequireCPUAccess != bRequireCPUAccess)
	{
		if (ensure(PerInstanceRenderData.IsValid()))
		{
			VOXEL_SCOPE_COUNTER("ReleasePerInstanceRenderData");
			ReleasePerInstanceRenderData();
		}

		VOXEL_SCOPE_COUNTER("InitPerInstanceRenderData");
		InitPerInstanceRenderData(true, BuiltData.InstanceBuffer.Get(), bRequireCPUAccess);
	}
	else
	{
		VOXEL_SCOPE_COUNTER("UpdateFromPreallocatedData");
		PerInstanceRenderData->UpdateFromPreallocatedData(*BuiltData.InstanceBuffer);
	}

	{
		VOXEL_SCOPE_COUNTER("AcceptPrebuiltTree");
		AcceptPrebuiltTree(BuiltData.ClusterTree, BuiltData.OcclusionLayerNum, NumInstances);
	}

	for (auto It = Voxel_TaskIdToNewInstancesBounds.CreateIterator(); It; ++It)
	{
		if (It.Key() <= BuiltData.UniqueId)
		{
			for (auto& Chunk : It.Value())
			{
				Voxel_RefreshPhysics(Chunk);
			}
			It.RemoveCurrent();
		}
	}

	Voxel_UpdateAllocatedMemory();
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_EnablePhysics(const FIntBox Chunk)
{
	VOXEL_FUNCTION_COUNTER();

	if (!ensure(!Voxel_InstanceBodies.Contains(Chunk))) return;
	auto& Bodies = Voxel_InstanceBodies.Add(Chunk);

	Voxel_EnablePhysicsImpl(Chunk, Bodies);

	Voxel_UpdateAllocatedMemory();
}

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_DisablePhysics(const FIntBox Chunk)
{
	VOXEL_FUNCTION_COUNTER();

	if (!ensure(Voxel_InstanceBodies.Contains(Chunk))) return;
	auto Bodies = Voxel_InstanceBodies.FindAndRemoveChecked(Chunk);
	
	Voxel_DisablePhysicsImpl(Bodies);

	Voxel_UpdateAllocatedMemory();
}

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_RefreshPhysics(const FIntBox& BoundsToUpdate)
{
	VOXEL_FUNCTION_COUNTER();

	for (auto& It : Voxel_InstanceBodies)
	{
		const auto& Chunk = It.Key;
		auto& Bodies = It.Value;
		if (Chunk.Intersect(BoundsToUpdate))
		{
			Voxel_DisablePhysicsImpl(Bodies);
			ensure(Bodies.Num() == 0);
			Voxel_EnablePhysicsImpl(Chunk, Bodies);
		}
	}

	Voxel_UpdateAllocatedMemory();
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_RemoveInstancesInArea(
	const FIntBox& VoxelBounds,
	const FVoxelConstDataAccelerator* Accelerator,
	EVoxelSpawnerActorSpawnType SpawnType,
	TArray<FVoxelSpawnerMatrix>& OutMatrices)
{
	VOXEL_FUNCTION_COUNTER();

	check(SpawnType == EVoxelSpawnerActorSpawnType::All || Accelerator);
	
	const auto ScaledBounds = VoxelBounds.Scale(Voxel_VoxelSize);

	TArray<int32> BuiltIndicesToClear;
	FIntBoxWithValidity BoundsToUpdate;

	const auto Lambda = [&](const int32 BuiltIndex)
	{
		if (Voxel_Mappings.GetUnbuiltIndex(BuiltIndex) == -1)
		{
			// Deleted
			return;
		}
		
		INC_DWORD_STAT_BY(STAT_VoxelHISMComponent_NumFloatingMeshChecked, 1);

		const FVoxelSpawnerMatrix Matrix = Voxel_BuiltMatrices[BuiltIndex];
		const FTransform LocalInstanceTransform = FTransform(Matrix.GetCleanMatrix());
		// Remove position offset to be on the voxel surface
		const FVector VoxelPosition = (LocalInstanceTransform.GetTranslation() - Matrix.GetPositionOffset()) / Voxel_VoxelSize;

		if (LocalInstanceTransform.GetScale3D().IsNearlyZero() || 
			!VoxelBounds.Contains(FIntBox(VoxelPosition)))
		{
			return;
		}

		if (SpawnType == EVoxelSpawnerActorSpawnType::All || Accelerator->GetFloatValue(VoxelPosition, 0) > 0)
		{
			OutMatrices.Add(Matrix);
			BuiltIndicesToClear.Add(BuiltIndex);
			BoundsToUpdate += FIntBox(VoxelPosition);
		}
	};
	if (!ensure(ClusterTreePtr.IsValid())) return;
	Voxel_IterateInstancesInBounds(*ClusterTreePtr, ScaledBounds, Lambda);

	if (BuiltIndicesToClear.Num() == 0)
	{
		return;
	}

	Voxel_SetInstancesScaleToZero(BuiltIndicesToClear);
	Voxel_RemoveInstancesFromSections(BuiltIndicesToClear);
	Voxel_RefreshPhysics(BoundsToUpdate.GetBox());
}

bool UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_RemoveInstanceByIndex(int32 InstanceIndex, FVoxelSpawnerMatrix& OutMatrix)
{
	VOXEL_FUNCTION_COUNTER();
	
	const int32 BuiltIndex = Voxel_Mappings.GetBuiltIndex(InstanceIndex);
	if (BuiltIndex == -1)
	{
		return false;
	}

	OutMatrix = Voxel_BuiltMatrices[BuiltIndex];

	Voxel_SetInstancesScaleToZero({ BuiltIndex });
	Voxel_RemoveInstancesFromSections({ BuiltIndex });

	const FIntBox MatrixBounds = FIntBox(FTransform(OutMatrix.GetCleanMatrix()).GetTranslation() / Voxel_VoxelSize);
	
	for (auto& It : Voxel_InstanceBodies)
	{
		const auto& Chunk = It.Key;
		auto& Bodies = It.Value;
		if (Chunk.Intersect(MatrixBounds))
		{
			for (int32 BodyIndex = 0; BodyIndex < Bodies.Num(); BodyIndex++)
			{
				auto* Body = Bodies[BodyIndex];
				if (Body->InstanceBodyIndex == InstanceIndex)
				{
					DEC_DWORD_STAT(STAT_VoxelHISMComponent_NumPhysicsBodies);
					Body->TermBody();
					delete Body;
					Bodies.RemoveAtSwap(BodyIndex);
					return true;
				}
			}
		}
	}

#if VOXEL_DEBUG
	for (auto& It : Voxel_InstanceBodies)
	{
		for (auto* Body : It.Value)
		{
			ensure(Body->InstanceBodyIndex != InstanceIndex);
		}
	}
#endif
	
	return true;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	VOXEL_FUNCTION_COUNTER();
	
	ensure((Voxel_TaskUniqueId != 0) == Voxel_TaskCancelCounterPtr.IsValid());
	
	Voxel_TaskUniqueId = 0;
	if (Voxel_TaskCancelCounterPtr.IsValid())
	{
		Voxel_TaskCancelCounterPtr->Increment();
		Voxel_TaskCancelCounterPtr.Reset();
	}
	
	DEC_DWORD_STAT_BY(STAT_VoxelHISMComponent_NumInstances, Voxel_UnbuiltMatrices.Num());

	// Free up memory
	Voxel_UnbuiltMatrices.Empty();
	Voxel_BuiltMatrices.Empty();
	Voxel_Mappings.InstancesToBuiltInstances.Empty();
	Voxel_Mappings.BuiltInstancesToInstances.Empty();
	Voxel_Mappings.DeletionsStack.Empty();
	Voxel_UnbuiltInstancesToClear.Empty();
	Voxel_TaskIdToNewInstancesBounds.Empty();
	Voxel_PendingNewInstancesBounds.Empty();
	
	Super::OnComponentDestroyed(bDestroyingHierarchy);
}

bool UVoxelHierarchicalInstancedStaticMeshComponent::ShouldCreatePhysicsState() const
{
	return UInstancedStaticMeshComponent::ShouldCreatePhysicsState();
}

void UVoxelHierarchicalInstancedStaticMeshComponent::OnDestroyPhysicsState()
{
	VOXEL_FUNCTION_COUNTER();
	
	Super::OnDestroyPhysicsState();

	for (auto& It : Voxel_InstanceBodies)
	{
		Voxel_DisablePhysicsImpl(It.Value);
	}
	Voxel_InstanceBodies.Empty();
}

void UVoxelHierarchicalInstancedStaticMeshComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	VOXEL_FUNCTION_COUNTER();
	
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (CVarShowHISMCollisions.GetValueOnGameThread())
	{
		if (!GetStaticMesh()) return;
		
		const FBox MeshBox = GetStaticMesh()->GetBounds().GetBox();
		for (auto& It : Voxel_InstanceBodies)
		{
			for (auto* Body : It.Value)
			{
				const FBox BodyBox = MeshBox.TransformBy(Body->GetUnrealWorldTransform());
				DrawDebugBox(
					GetWorld(),
					BodyBox.GetCenter(),
					BodyBox.GetExtent(),
					FColor::Red,
					false,
					DeltaTime * 2);
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

int32 UVoxelHierarchicalInstancedStaticMeshComponent::FVoxelMappings::GetBuiltIndex(int32 UnbuiltIndex) const
{
	// Reverse back the changes
	for (int32 StackIndex = DeletionsStack.Num() - 1; StackIndex >= 0; StackIndex--)
	{
		auto& Deletion = DeletionsStack[StackIndex];
		if (Deletion.StartIndex <= UnbuiltIndex) // If we were affected by the deletion
		{
			UnbuiltIndex += Deletion.Num; // Translate back
		}
	}
	if (InstancesToBuiltInstances.IsValidIndex(UnbuiltIndex))
	{
		return InstancesToBuiltInstances[UnbuiltIndex];
	}
	else
	{
		return -1;
	}
}

int32 UVoxelHierarchicalInstancedStaticMeshComponent::FVoxelMappings::GetUnbuiltIndex(const int32 BuiltIndex) const
{
	if (!BuiltInstancesToInstances.IsValidIndex(BuiltIndex))
	{
		return -1;
	}
	
	int32 UnbuiltIndex = BuiltInstancesToInstances[BuiltIndex];
	
	// The UnbuiltIndex is the one of the last tree build: need to apply deletions to it
	for (int32 StackIndex = 0; StackIndex < DeletionsStack.Num(); StackIndex++)
	{
		auto& Deletion = DeletionsStack[StackIndex];
		if (Deletion.StartIndex <= UnbuiltIndex) 
		{
			if (UnbuiltIndex < Deletion.StartIndex + Deletion.Num)
			{
				// Got deleted
				return -1;
			}

			// Apply the deletion
			UnbuiltIndex -= Deletion.Num;
		}
	}
	
	ensureVoxelSlowNoSideEffects(GetBuiltIndex(UnbuiltIndex) == BuiltIndex);
	return UnbuiltIndex;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_UpdateAllocatedMemory()
{
	VOXEL_FUNCTION_COUNTER();
	
	DEC_VOXEL_MEMORY_STAT_BY(STAT_VoxelHISMMemory, Voxel_AllocatedMemory);
	
	Voxel_AllocatedMemory = 0;
	Voxel_AllocatedMemory += Voxel_UnbuiltMatrices.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_BuiltMatrices.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_Mappings.InstancesToBuiltInstances.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_Mappings.BuiltInstancesToInstances.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_Mappings.DeletionsStack.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_UnbuiltInstancesToClear.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_TaskIdToNewInstancesBounds.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_PendingNewInstancesBounds.GetAllocatedSize();
	Voxel_AllocatedMemory += Voxel_InstanceBodies.GetAllocatedSize();

	INC_VOXEL_MEMORY_STAT_BY(STAT_VoxelHISMMemory, Voxel_AllocatedMemory);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_ScheduleBuildTree()
{
	VOXEL_FUNCTION_COUNTER();
	
	if (Voxel_BuildDelay <= 0)
	{
		Voxel_StartBuildTree();
	}
	else
	{
		auto& TimerManager = GetWorld()->GetTimerManager();
		TimerManager.SetTimer(
			Voxel_TimerHandle,
			this,
			&UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_StartBuildTree,
			Voxel_BuildDelay,
			false);
	}
}

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_RemoveInstancesFromSections(const TArray<int32>& BuiltIndices)
{
	VOXEL_FUNCTION_COUNTER();

#if VOXEL_DEBUG
	for (int32 Index = 0; Index < Voxel_Sections.Num() - 1; Index++)
	{
		ensure(Voxel_Sections[Index]->StartIndex < Voxel_Sections[Index + 1]->StartIndex);
		ensure(Voxel_Sections[Index]->StartIndex + Voxel_Sections[Index]->Num == Voxel_Sections[Index + 1]->StartIndex);
	}
	ensure(Voxel_UnbuiltMatrices.Num() == 0 || Voxel_Sections.Last()->StartIndex + Voxel_Sections.Last()->Num == Voxel_UnbuiltMatrices.Num());
#endif
	
	for (int32 BuiltIndex : BuiltIndices)
	{
		const int32 UnbuiltIndex = Voxel_Mappings.GetUnbuiltIndex(BuiltIndex);
		if (!ensure(UnbuiltIndex != -1)) continue;

		const int32 SectionIndex = Algo::UpperBoundBy(Voxel_Sections, UnbuiltIndex, [](auto& Section) { return Section->StartIndex; });
		
		auto& Section = *Voxel_Sections[SectionIndex - 1];
		
		if (!ensure(Section.StartIndex <= UnbuiltIndex && UnbuiltIndex < Section.StartIndex + Section.Num)) continue;

		Section.RemovedIndices.Add(UnbuiltIndex - Section.StartIndex);
	}
}

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_SetInstancesScaleToZero(const TArray<int32>& BuiltIndices)
{
	VOXEL_FUNCTION_COUNTER();

	const auto InstanceBuffer = PerInstanceRenderData.IsValid() ? PerInstanceRenderData->InstanceBuffer_GameThread : nullptr;
	ensure(InstanceBuffer.IsValid() || Voxel_TaskUniqueId > 0);
	for (int32 BuiltIndex : BuiltIndices)
	{
		const int32 UnbuiltIndex = Voxel_Mappings.GetUnbuiltIndex(BuiltIndex);
		check(UnbuiltIndex >= 0);
		
		if (InstanceBuffer.IsValid())
		{
			InstanceBuffer->SetInstance(BuiltIndex, EmptyMatrix, 0);
		}
		ensure(Voxel_BuiltMatrices[BuiltIndex] == Voxel_UnbuiltMatrices[UnbuiltIndex]);
		Voxel_BuiltMatrices[BuiltIndex] = FVoxelSpawnerMatrix(EmptyMatrix);
		Voxel_UnbuiltMatrices[UnbuiltIndex] = FVoxelSpawnerMatrix(EmptyMatrix);

		if (Voxel_TaskUniqueId > 0)
		{
			Voxel_UnbuiltInstancesToClear.Add(UnbuiltIndex);
		}
	}

	if (InstanceBuffer.IsValid())
	{
		ENQUEUE_RENDER_COMMAND(UVoxelHierarchicalInstancedStaticMeshComponent_UpdateBuffer)(
			[PerInstanceRenderData = PerInstanceRenderData](FRHICommandListImmediate& RHICmdList)
			{
				PerInstanceRenderData->InstanceBuffer.InstanceData = PerInstanceRenderData->InstanceBuffer_GameThread;
				PerInstanceRenderData->InstanceBuffer.UpdateRHI();
			}
		);
		MarkRenderStateDirty();
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_EnablePhysicsImpl(const FIntBox& Chunk, TArray<FBodyInstance*>& OutBodies) const
{
	VOXEL_FUNCTION_COUNTER();
	ensure(bPhysicsStateCreated);
	
	// We want this function to be const
	auto* const Component = const_cast<UPrimitiveComponent*>(static_cast<const UPrimitiveComponent*>(this));
	
	UBodySetup* const BodySetup = Component->GetBodySetup();
	if (!ensure(BodySetup)) return;

	const auto ScaledBounds = Chunk.Scale(Voxel_VoxelSize);

	TArray<FTransform> Transforms;
	const auto Lambda = [&](const int32 BuiltIndex)
	{
		const FTransform LocalInstanceTransform = FTransform(Voxel_BuiltMatrices[BuiltIndex].GetCleanMatrix());
		const FTransform GlobalInstanceTransform = LocalInstanceTransform * GetComponentTransform();

		if (GlobalInstanceTransform.GetScale3D().IsNearlyZero() ||
			!ScaledBounds.ContainsFloat(LocalInstanceTransform.GetTranslation()))
		{
			return;
		}

		FBodyInstance* Instance = new FBodyInstance();
		INC_DWORD_STAT(STAT_VoxelHISMComponent_NumPhysicsBodies);

		Instance->CopyBodyInstancePropertiesFrom(&BodyInstance);
		Instance->bAutoWeld = false;

		// Make sure we never enable bSimulatePhysics for ISMComps
		Instance->bSimulatePhysics = false;

		// Set the body index to the UNBUILT index
		const int32 UnbuiltIndex = Voxel_Mappings.GetUnbuiltIndex(BuiltIndex);
		if (UnbuiltIndex == -1)
		{
			// Deleted
			return;
		}
		Instance->InstanceBodyIndex = UnbuiltIndex;
		ensure(Voxel_UnbuiltMatrices[Instance->InstanceBodyIndex] == Voxel_BuiltMatrices[BuiltIndex]);

		OutBodies.Add(Instance);
		Transforms.Add(GlobalInstanceTransform);
	};
	
	if (!ensure(ClusterTreePtr.IsValid())) return;
	Voxel_IterateInstancesInBounds(*ClusterTreePtr, ScaledBounds, Lambda);

	if (OutBodies.Num() == 0) return;

	VOXEL_SCOPE_COUNTER("InitStaticBodies");
	FBodyInstance::InitStaticBodies(OutBodies, Transforms, BodySetup, Component, GetWorld()->GetPhysicsScene());
}

void UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_DisablePhysicsImpl(TArray<FBodyInstance*>& Bodies) const
{
	VOXEL_FUNCTION_COUNTER();
	
	for (auto* Body : Bodies)
	{
		DEC_DWORD_STAT(STAT_VoxelHISMComponent_NumPhysicsBodies);
		Body->TermBody();
		delete Body;
	}
	Bodies.Reset();
}
#endif