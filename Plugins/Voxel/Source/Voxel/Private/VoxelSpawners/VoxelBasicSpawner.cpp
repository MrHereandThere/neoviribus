// Copyright 2020 Phyronnaz

#if VOXEL_PLUGIN_PRO
#include "VoxelSpawners/VoxelBasicSpawner.h"

FVoxelBasicSpawnerProxy::FVoxelBasicSpawnerProxy(UVoxelBasicSpawner* Spawner, FVoxelSpawnerManager& Manager, EVoxelSpawnerProxyType Type, uint32 Seed)
	: FVoxelSpawnerProxy(Spawner, Manager, Type, Seed)
	, GroundSlopeAngle(Spawner->GroundSlopeAngle)
	, Scaling(Spawner->Scaling)
	, RotationAlignment(Spawner->RotationAlignment)
	, bRandomYaw(Spawner->bRandomYaw)
	, RandomPitchAngle(Spawner->RandomPitchAngle)
	, PositionOffset(Spawner->PositionOffset)
	, RotationOffset(Spawner->RotationOffset)
{
}
#endif