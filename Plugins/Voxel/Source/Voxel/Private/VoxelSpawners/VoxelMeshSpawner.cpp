// Copyright 2020 Phyronnaz

#include "VoxelSpawners/VoxelMeshSpawner.h"
#include "VoxelSpawners/VoxelSpawnerManager.h"
#include "VoxelSpawners/VoxelInstancedMeshManager.h"
#include "VoxelSpawners/VoxelSpawnerGroup.h"
#include "VoxelData/VoxelData.h"
#include "VoxelData/VoxelDataAccelerator.h"
#include "VoxelWorldInterface.h"
#include "VoxelMessages.h"
#include "VoxelWorldGeneratorUtilities.h"

#include "Async/Async.h"
#include "TimerManager.h"
#include "Engine/StaticMesh.h"

#if VOXEL_PLUGIN_PRO
FVoxelMeshSpawnerProxyResult::FVoxelMeshSpawnerProxyResult(const FVoxelMeshSpawnerProxy& Proxy)
	: FVoxelSpawnerProxyResult(EVoxelSpawnerProxyType::MeshSpawner, Proxy)
{
}

FVoxelMeshSpawnerProxyResult::~FVoxelMeshSpawnerProxyResult()
{
}

void FVoxelMeshSpawnerProxyResult::Init(const FIntBox& InBounds, TArray<FVoxelSpawnerMatrix>&& InMatrices)
{
	Bounds = InBounds;
	Matrices = MoveTemp(InMatrices);
}

void FVoxelMeshSpawnerProxyResult::CreateImpl()
{
	check(IsInGameThread());

	const auto& MeshProxy = static_cast<const FVoxelMeshSpawnerProxy&>(Proxy);

	auto& MeshManager = *MeshProxy.Manager.Settings.MeshManager;
	if (MeshProxy.bAlwaysSpawnActor)
	{
		// Only spawn actors the first time
		if (!IsDirty())
		{
			TArray<AVoxelSpawnerActor*> Actors;
			MeshManager.SpawnActors(MeshProxy.InstanceSettings, Matrices, Actors);
			MarkDirty();
		}
	}
	else
	{
		if (Matrices.Num() > 0)
		{
			// Matrices can be empty if all instances were removed
			ensure(!InstancesRef.IsValid());
			const auto Ref = MeshManager.AddInstances(MeshProxy.InstanceSettings, Matrices, Bounds);
			InstancesRef = MakeUnique<FVoxelInstancedMeshInstancesRef>(Ref);
		}
	}
}

void FVoxelMeshSpawnerProxyResult::DestroyImpl()
{
	check(IsInGameThread());
	ApplyRemovedIndices();
		
	const auto& MeshProxy = static_cast<const FVoxelMeshSpawnerProxy&>(Proxy);
	if (MeshProxy.bAlwaysSpawnActor)
	{
		// Do nothing
		ensure(IsDirty());
	}
	else
	{
		auto& MeshManager = *MeshProxy.Manager.Settings.MeshManager;
		if (InstancesRef.IsValid())
		{
			MeshManager.RemoveInstances(*InstancesRef);
			InstancesRef.Reset();
		}
	}
}

void FVoxelMeshSpawnerProxyResult::SerializeImpl(FArchive& Ar, int32 VoxelCustomVersion)
{
	check(IsInGameThread());
	ApplyRemovedIndices();
	
	Ar << Bounds;
	Ar << Matrices;
}

uint32 FVoxelMeshSpawnerProxyResult::GetAllocatedSize()
{
	return sizeof(*this) + Matrices.GetAllocatedSize();
}

void FVoxelMeshSpawnerProxyResult::ApplyRemovedIndices()
{
	const auto& MeshProxy = static_cast<const FVoxelMeshSpawnerProxy&>(Proxy);
	auto& MeshManager = *MeshProxy.Manager.Settings.MeshManager;

	if (MeshProxy.bAlwaysSpawnActor)
	{
		// Do nothing
	}
	else
	{
		if (InstancesRef.IsValid())
		{
			auto RemovedIndices = MeshManager.GetRemovedIndices(*InstancesRef);
			RemovedIndices.Sort([](int32 A, int32 B) { return A > B; }); // Need to sort in decreasing order for the RemoveAtSwap
			for (int32 RemovedIndex : RemovedIndices)
			{
				Matrices.RemoveAtSwap(RemovedIndex, 1, false);
			}
			Matrices.Shrink();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelMeshSpawnerProxy::FVoxelMeshSpawnerProxy(UVoxelMeshSpawnerBase* Spawner, TWeakObjectPtr<UStaticMesh> Mesh, FVoxelSpawnerManager& Manager, uint32 Seed)
	: FVoxelBasicSpawnerProxy(Spawner, Manager, EVoxelSpawnerProxyType::MeshSpawner, Seed)
	, InstanceSettings({ Mesh, Spawner->InstancedMeshSettings, Spawner->ActorSettings })
	, InstanceRandom(Spawner->InstanceRandom)
	, ColorOutputName(Spawner->ColorOutputName)
	, bAlwaysSpawnActor(Spawner->bAlwaysSpawnActor)
	, FloatingDetectionOffset(Spawner->FloatingDetectionOffset)
{
	auto& WorldGenerator = *Manager.Settings.Data->WorldGenerator;

	if (InstanceRandom == EVoxelMeshSpawnerInstanceRandom::ColorOutput &&
		!WorldGenerator.GetOutputsPtrMap<FColor>().Contains(ColorOutputName))
	{
		FVoxelMessages::Error(
			"Mesh Spawner: Color output for instance random not found: " +
			FVoxelUtilities::GetMissingWorldGeneratorOutputErrorString<FColor>(ColorOutputName, WorldGenerator),
			Spawner);
	}
}

TUniquePtr<FVoxelSpawnerProxyResult> FVoxelMeshSpawnerProxy::ProcessHits(
	const FIntBox& Bounds, 
	const TArray<FVoxelSpawnerHit>& Hits, 
	const FVoxelConstDataAccelerator& Accelerator) const
{
	check(Hits.Num() > 0);
	
	const uint32 Seed = Bounds.GetMurmurHash() ^ SpawnerSeed;
	auto& WorldGenerator = *Manager.Settings.Data->WorldGenerator;
	const float VoxelSize = Manager.Settings.VoxelSize;

	const FRandomStream Stream(Seed);

	TArray<FVoxelSpawnerMatrix> Matrices;
	Matrices.Reserve(Hits.Num());

	const auto ColorOutputPtr = WorldGenerator.GetOutputsPtrMap<FColor>().FindRef(ColorOutputName);
	
	for (auto& Hit : Hits)
	{
		const auto& Position = Hit.Position;
		const auto& Normal = Hit.Normal;
		const auto WorldUp = WorldGenerator.GetUpVector(Position);

		if (!CanSpawn(Normal, WorldUp))
		{
			continue;
		}

		const FMatrix Matrix = GetMatrixWithoutOffsets(Stream, Normal, WorldUp);

		const FVector RotatedPositionOffset = Matrix.TransformVector(PositionOffset);

		FMatrix Transform = Matrix;
		Transform *= RotationOffset;
		Transform = Transform.ConcatTranslation(RotatedPositionOffset + VoxelSize * Position);
		FVoxelSpawnerMatrix SpawnerMatrix(Transform);
		
		if (InstanceRandom == EVoxelMeshSpawnerInstanceRandom::Random)
		{
			SpawnerMatrix.SetRandomInstanceId(Stream.GetFraction());
		}
		else if (InstanceRandom == EVoxelMeshSpawnerInstanceRandom::VoxelMaterial)
		{
			// Note: instead of RoundToInt, should maybe use the nearest voxel that's not empty?
			const auto Material = Accelerator.GetMaterial(FVoxelUtilities::RoundToInt(Position), 0);
			SpawnerMatrix.SetRandomInstanceId(Material.GetPackedInt());
		}
		else
		{
			check(InstanceRandom == EVoxelMeshSpawnerInstanceRandom::ColorOutput);
			const auto Color =
				ColorOutputPtr
				? (WorldGenerator.*ColorOutputPtr)(Position.X, Position.Y, Position.Z, 0, FVoxelItemStack::Empty)
				: FColor::Black;
			SpawnerMatrix.SetRandomInstanceId(FVoxelMaterial::CreateFromColor(Color).GetPackedInt());
		}

		const FVector RotatedFloatingDetectionOffset = Matrix.TransformVector(FloatingDetectionOffset);
		SpawnerMatrix.SetPositionOffset(RotatedPositionOffset - RotatedFloatingDetectionOffset);
		
		Matrices.Add(SpawnerMatrix);
	}

	if (Matrices.Num() == 0) 
	{
		return nullptr;
	}
	else
	{
		Matrices.Shrink();
		
		auto Result = MakeUnique<FVoxelMeshSpawnerProxyResult>(*this);
		Result->Init(Bounds, MoveTemp(Matrices));
		return Result;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

inline TArray<TVoxelSharedPtr<FVoxelMeshSpawnerProxy>> CreateMeshProxies(UVoxelMeshSpawnerGroup* Spawner, FVoxelSpawnerManager& Manager)
{
	TArray<TVoxelSharedPtr<FVoxelMeshSpawnerProxy>> Result;
	uint32 Seed = 1;
	for (auto* Mesh : Spawner->Meshes)
	{
		Result.Add(MakeVoxelShared<FVoxelMeshSpawnerProxy>(Spawner, Mesh, Manager, Seed++));
	}
	return Result;
}

FVoxelMeshSpawnerGroupProxy::FVoxelMeshSpawnerGroupProxy(UVoxelMeshSpawnerGroup* Spawner, FVoxelSpawnerManager& Manager)
	: FVoxelSpawnerProxy(Spawner, Manager, EVoxelSpawnerProxyType::SpawnerGroup, 0)
	, Proxies(CreateMeshProxies(Spawner, Manager))
{
}

TUniquePtr<FVoxelSpawnerProxyResult> FVoxelMeshSpawnerGroupProxy::ProcessHits(
	const FIntBox& Bounds,
	const TArray<FVoxelSpawnerHit>& Hits,
	const FVoxelConstDataAccelerator& Accelerator) const
{
	TArray<TUniquePtr<FVoxelSpawnerProxyResult>> Results;
	
	const int32 NumHitsPerProxy = FVoxelUtilities::DivideCeil(Hits.Num(), Proxies.Num());
	int32 HitsStartIndex = 0;
	for (auto& Proxy : Proxies)
	{
		const int32 HitsEndIndex = FMath::Min(HitsStartIndex + NumHitsPerProxy, Hits.Num());
		if (HitsStartIndex < HitsEndIndex) 
		{
			TArray<FVoxelSpawnerHit> ProxyHits(Hits.GetData() + HitsStartIndex, HitsEndIndex - HitsStartIndex);
			auto Result = Proxy->ProcessHits(Bounds, ProxyHits, Accelerator);
			if (Result.IsValid())
			{
				Results.Emplace(MoveTemp(Result));
			}
		}
		HitsStartIndex = HitsEndIndex;
	}

	if (Results.Num() == 0)
	{
		return nullptr;
	}
	else
	{
		auto Result = MakeUnique<FVoxelSpawnerGroupProxyResult>(*this);
		Result->Init(MoveTemp(Results));
		return Result;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

TVoxelSharedRef<FVoxelSpawnerProxy> UVoxelMeshSpawner::GetSpawnerProxy(FVoxelSpawnerManager& Manager)
{
	if (!Mesh && !bAlwaysSpawnActor)
	{
		FVoxelMessages::Error("Invalid mesh!", this);
	}
	return MakeVoxelShared<FVoxelMeshSpawnerProxy>(this, Mesh, Manager, 0);
}

///////////////////////////////////////////////////////////////////////////////

TVoxelSharedRef<FVoxelSpawnerProxy> UVoxelMeshSpawnerGroup::GetSpawnerProxy(FVoxelSpawnerManager& Manager)
{
	if (!bAlwaysSpawnActor)
	{
		for (auto* Mesh : Meshes)
		{
			if (!Mesh)
			{
				FVoxelMessages::Error("Invalid mesh!", this);
			}
		}
	}
	return MakeVoxelShared<FVoxelMeshSpawnerGroupProxy>(this, Manager);
}

float UVoxelMeshSpawnerGroup::GetDistanceBetweenInstancesInVoxel() const
{
	// Scale it to account for instances split between meshes
	return DistanceBetweenInstancesInVoxel / FMath::Max(1, Meshes.Num());
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#endif

void UVoxelMeshSpawnerBase::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);
	
	if (!IsTemplate())
	{
		ActorSettings.BodyInstance.FixupData(this);
		InstancedMeshSettings.BodyInstance.FixupData(this);
	}
}