// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"

struct FVoxelCancelCounter;
struct FIntBox;
struct FVoxelSpawnerThreadSafeConfig;
struct FVoxelSpawnerHit;
class FVoxelSpawnerRayHandler;
class FVoxelConstDataAccelerator;
class UVoxelSpawner;

namespace FVoxelSpawnerUtilities
{
	void SpawnWithRays(
		const FVoxelCancelCounter& CancelCounter,
		const FVoxelConstDataAccelerator& Accelerator,
		const FVoxelSpawnerThreadSafeConfig& ThreadSafeConfig,
		int32 RayGroupIndex,
		const TArray<int32>& ElementsToSpawn,
		const FIntBox& Bounds,
		const FVoxelSpawnerRayHandler& RayHandler,
		TMap<int32, TArray<FVoxelSpawnerHit>>& OutHits);

	void SpawnWithHeight(
		const FVoxelCancelCounter& CancelCounter,
		const FVoxelConstDataAccelerator& Accelerator,
		const FVoxelSpawnerThreadSafeConfig& ThreadSafeConfig,
		int32 HeightGroupIndex,
		const TArray<int32>& ElementsToSpawn,
		const FIntBox& Bounds,
		TMap<int32, TArray<FVoxelSpawnerHit>>& OutHits);
}
#endif