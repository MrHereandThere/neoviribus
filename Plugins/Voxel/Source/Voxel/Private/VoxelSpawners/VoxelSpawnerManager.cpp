// Copyright 2020 Phyronnaz

#if VOXEL_PLUGIN_PRO
#include "VoxelSpawners/VoxelSpawnerManager.h"
#include "VoxelSpawners/VoxelSpawnerUtilities.h"
#include "VoxelSpawners/VoxelSpawner.h"
#include "VoxelSpawners/VoxelSpawnerEmbreeRayHandler.h"
#include "VoxelSpawners/VoxelSpawnerRayHandler.h"
#include "VoxelSpawners/VoxelEmptySpawner.h"
#include "VoxelEvents/VoxelEventManager.h"
#include "VoxelData/VoxelData.h"
#include "VoxelData/VoxelDataAccelerator.h"
#include "VoxelRender/IVoxelRenderer.h"
#include "VoxelDebug/VoxelDebugManager.h"
#include "VoxelWorld.h"
#include "IVoxelPool.h"
#include "VoxelAsyncWork.h"
#include "VoxelPriorityHandler.h"
#include "VoxelMessages.h"
#include "VoxelCustomVersion.h"
#include "VoxelThreadingUtilities.h"
#include "VoxelIntVectorUtilities.h"
#include "VoxelSerializationUtilities.h"
#include "VoxelWorldGeneratorUtilities.h"

#include "Async/Async.h"
#include "DrawDebugHelpers.h"
#include "Serialization/BufferArchive.h"
#include "Serialization/MemoryReader.h"

DEFINE_VOXEL_MEMORY_STAT(STAT_VoxelSpawnerManagerMemory);

static TAutoConsoleVariable<int32> CVarShowVoxelSpawnerRays(
	TEXT("voxel.spawners.ShowRays"),
	0,
	TEXT("If true, will show the voxel spawner rays"),
	ECVF_Default);

static TAutoConsoleVariable<int32> CVarShowVoxelSpawnerHits(
	TEXT("voxel.spawners.ShowHits"),
	0,
	TEXT("If true, will show the voxel spawner rays hits"),
	ECVF_Default);

static TAutoConsoleVariable<int32> CVarShowVoxelSpawnerPositions(
	TEXT("voxel.spawners.ShowPositions"),
	0,
	TEXT("If true, will show the positions sent to spawners"),
	ECVF_Default);

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

class FVoxelSpawnerTask : public FVoxelAsyncWork
{
public:
	const TVoxelWeakPtr<FVoxelSpawnerManager> SpawnerManagerPtr;
	const bool bIsHeightTask;
	const FIntBox Bounds;
	const int32 GroupIndex;
	const TArray<int32> ElementsToSpawn;
	const FVoxelCancelCounter CancelCounter;
	const FVoxelPriorityHandler PriorityHandler;

	FVoxelSpawnerTask(
		FVoxelSpawnerManager& SpawnerManager,
		bool bIsHeightTask,
		const FIntBox& Bounds,
		int32 GroupIndex, 
		const TArray<int32>& ElementsToSpawn,
		const FVoxelCancelCounter& CancelCounter)
		: FVoxelAsyncWork(STATIC_FNAME("Spawner Task"), SpawnerManager.Settings.PriorityDuration, true)
		, SpawnerManagerPtr(SpawnerManager.AsShared())
		, bIsHeightTask(bIsHeightTask)
		, Bounds(Bounds)
		, GroupIndex(GroupIndex)
		, ElementsToSpawn(ElementsToSpawn)
		, CancelCounter(CancelCounter)
		, PriorityHandler(Bounds, SpawnerManager.Settings.Renderer->GetInvokersPositionsForPriorities())
	{
		SpawnerManager.TaskCounter.Increment();
		SpawnerManager.UpdateTaskCount();
	}

	virtual void DoWork() override
	{
		auto SpawnerManager = SpawnerManagerPtr.Pin();
		if (!SpawnerManager.IsValid()) return;

		if (bIsHeightTask)
		{
			SpawnerManager->SpawnHeightGroup_AnyThread(*this);
		}
		else
		{
			SpawnerManager->SpawnRayGroup_AnyThread(*this);
		}

		SpawnerManager->TaskCounter.Decrement();
		SpawnerManager->UpdateTaskCount();

		FVoxelUtilities::DeleteOnGameThread_AnyThread(SpawnerManager);
	}
	virtual uint32 GetPriority() const override
	{
		return PriorityHandler.GetPriority();
	}

private:
	~FVoxelSpawnerTask() = default;

	template<typename T>
	friend struct TVoxelAsyncWorkDelete;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelSpawnerSettings::FVoxelSpawnerSettings(
	const AVoxelWorld* World,
	const TVoxelSharedRef<IVoxelPool>& Pool,
	const TVoxelSharedRef<FVoxelDebugManager>& DebugManager,
	const TVoxelSharedRef<FVoxelData>& Data,
	const TVoxelSharedRef<IVoxelLODManager>& LODManager,
	const TVoxelSharedRef<IVoxelRenderer>& Renderer,
	const TVoxelSharedRef<FVoxelInstancedMeshManager>& MeshManager,
	const TVoxelSharedRef<FVoxelEventManager>& EventManager)
	: VoxelWorldInterface(World)
	, Pool(Pool)
	, DebugManager(DebugManager)
	, Data(Data)
	, MeshManager(MeshManager)
	, EventManager(EventManager)
	, LODManager(LODManager)
	, Renderer(Renderer)
	, Config(World->SpawnerConfig)
	, VoxelSize(World->VoxelSize)
	, Seeds(World->Seeds)
	, PriorityDuration(World->PriorityDuration)
{
}

TVoxelSharedRef<FVoxelSpawnerManager> FVoxelSpawnerManager::Create(const FVoxelSpawnerSettings& Settings)
{
	VOXEL_FUNCTION_COUNTER();
	
	TVoxelSharedRef<FVoxelSpawnerManager> Manager = MakeShareable(new FVoxelSpawnerManager(Settings));

	if (!Settings.Config.IsValid()) return Manager;

	UVoxelSpawnerConfig& Config = *Settings.Config;
	FVoxelSpawnerThreadSafeConfig& ThreadSafeConfig = Manager->ThreadSafeConfig;
	
	ThreadSafeConfig.WorldType = Config.WorldType;
	ThreadSafeConfig.RayGroups = Config.RaySpawners;
	ThreadSafeConfig.HeightGroups = Config.HeightSpawners;

	const auto ResetManager = [&]()
	{
		ThreadSafeConfig.RayGroups.Reset();
		ThreadSafeConfig.HeightGroups.Reset();
	};
	
	TSet<UVoxelSpawner*> Spawners;
	// Setup elements, and gather their spawners
	{
		const auto& WorldGenerator = *Settings.Data->WorldGenerator;

		const auto CheckFloatOutputExists = [&](FName Name)
		{
			if (!WorldGenerator.GetOutputsPtrMap<v_flt>().Contains(Name))
			{
				FVoxelMessages::Warning(
					FVoxelUtilities::GetMissingWorldGeneratorOutputErrorString<v_flt>(Name, WorldGenerator),
					&Config);
			}
		};
		const auto SetupElement = [&](auto& Element)
		{
			if (!Element.Spawner)
			{
				FVoxelMessages::Error("Spawner is null!", &Config);
				return false;
			}

			Spawners.Add(Element.Spawner);

			Element.DistanceBetweenInstancesInVoxel = Element.Spawner->GetDistanceBetweenInstancesInVoxel();

			if (Element.DensityGraphOutputName.Name != STATIC_FNAME("Constant 0") &&
				Element.DensityGraphOutputName.Name != STATIC_FNAME("Constant 1"))
			{
				CheckFloatOutputExists(Element.DensityGraphOutputName);
			}

			if (const int32* Seed = Settings.Seeds.Find(Element.Advanced.SeedName))
			{
				Element.FinalSeed = *Seed;
			}
			else
			{
				Element.FinalSeed = Element.Advanced.DefaultSeed;
			}

			return true;
		};
		
		for (auto& RayGroup : ThreadSafeConfig.RayGroups)
		{
			for (auto& RayElement : RayGroup.Spawners)
			{
				if (!SetupElement(RayElement))
				{
					ResetManager();
					return Manager;
				}
			}
		}
		for (auto& HeightGroup : ThreadSafeConfig.HeightGroups)
		{
			CheckFloatOutputExists(HeightGroup.HeightGraphOutputName);
			for (auto& HeightElement : HeightGroup.Spawners)
			{
				if (!SetupElement(HeightElement))
				{
					ResetManager();
					return Manager;
				}
			}
		}
	}

	// Gather all the spawners and the ones they reference
	{
		TArray<UVoxelSpawner*> QueuedSpawners = Spawners.Array();
		TSet<UVoxelSpawner*> ProcessedSpawners;

		while (QueuedSpawners.Num() > 0)
		{
			auto* Spawner = QueuedSpawners.Pop();
			if (ProcessedSpawners.Contains(Spawner)) continue;
			ProcessedSpawners.Add(Spawner);

			TSet<UVoxelSpawner*> NewSpawners;
			if (!Spawner->GetSpawners(NewSpawners))
			{
				ResetManager();
				return Manager;
			}
			QueuedSpawners.Append(NewSpawners.Array());
		}

		Spawners = MoveTemp(ProcessedSpawners);
	}

	// Create the proxies
	for (auto* Spawner : Spawners)
	{
		check(Spawner);
		Manager->SpawnersMap.Add(Spawner, Spawner->GetSpawnerProxy(*Manager));
	}

	// Call post spawn
	for (auto& It : Manager->SpawnersMap)
	{
		It.Value->PostSpawn();
	}

	const auto GetProxiesFromGroup = [&](auto& Group)
	{
		TArray<FVoxelSpawnerProxy*> Proxies;
		for (auto& Spawner : Group.Spawners)
		{
			Proxies.Add(Manager->SpawnersMap[Spawner.Spawner].Get());
		}
		return Proxies;
	};

	// Bind delegates
	for (int32 ElementIndex = 0; ElementIndex < ThreadSafeConfig.HeightGroups.Num(); ElementIndex++)
	{
		FVoxelSpawnerConfigHeightGroup& HeightGroup = ThreadSafeConfig.HeightGroups[ElementIndex];
		
		Manager->HeightGroupsData.Emplace(HeightGroup.ChunkSize, GetProxiesFromGroup(HeightGroup));

		Settings.EventManager->BindEvent(
			true,
			HeightGroup.ChunkSize,
			HeightGroup.GenerationDistanceInChunks,
			FChunkDelegate::CreateThreadSafeSP(Manager, &FVoxelSpawnerManager::SpawnGroup_GameThread, ElementIndex, true),
			FChunkDelegate::CreateThreadSafeSP(Manager, &FVoxelSpawnerManager::DestroyGroup_GameThread, ElementIndex, true));
	}
	for (int32 ElementIndex = 0; ElementIndex < ThreadSafeConfig.RayGroups.Num(); ElementIndex++)
	{
		FVoxelSpawnerConfigRayGroup& RayGroup = ThreadSafeConfig.RayGroups[ElementIndex];
		
		ensure(RayGroup.ChunkSize == RENDER_CHUNK_SIZE << RayGroup.LOD);
		Manager->RayGroupsData.Emplace(RayGroup.ChunkSize, GetProxiesFromGroup(RayGroup));
		
		Settings.EventManager->BindEvent(
			true,
			RayGroup.ChunkSize,
			RayGroup.GenerationDistanceInChunks,
			FChunkDelegate::CreateThreadSafeSP(Manager, &FVoxelSpawnerManager::SpawnGroup_GameThread, ElementIndex, false),
			FChunkDelegate::CreateThreadSafeSP(Manager, &FVoxelSpawnerManager::DestroyGroup_GameThread, ElementIndex, false));
	}
	
	return Manager;
}

void FVoxelSpawnerManager::Destroy()
{
	const auto CancelGroupsTasks = [](TArray<FSpawnerGroupData>& Groups)
	{
		for (auto& Group : Groups)
		{
			FScopeLock Lock(&Group.Section);

			for (auto& It : Group.ChunksData)
			{
				It.Value.CancelTasks();
			}
		}
	};
	CancelGroupsTasks(HeightGroupsData);
	CancelGroupsTasks(RayGroupsData);

	StopTicking();
}

FVoxelSpawnerManager::~FVoxelSpawnerManager()
{
	ensure(IsInGameThread());
}

FVoxelSpawnerManager::FVoxelSpawnerManager(const FVoxelSpawnerSettings& Settings)
	: Settings(Settings)
{
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

TVoxelSharedPtr<FVoxelSpawnerProxy> FVoxelSpawnerManager::GetSpawner(UVoxelSpawner* Spawner) const
{
	return SpawnersMap.FindRef(Spawner);
}

void FVoxelSpawnerManager::Regenerate(const FIntBox& Bounds)
{
	VOXEL_FUNCTION_COUNTER();

	IterateResultsInBounds(
		Bounds,
		[&](TVoxelSharedPtr<FVoxelSpawnerProxyResult>& Result)
		{
			if (Result.IsValid() && !Result->NeedsToBeSaved())
			{
				if (Result->IsCreated())
				{
					Result->Destroy();
				}
				else
				{
					FScopeLock Lock(&CreateQueueSection);
					ensure(CreateQueue.RemoveSwap(Result) == 1);
				}
				Result.Reset();
				return true;
			}
			else
			{
				return false;
			}
		});
}

void FVoxelSpawnerManager::MarkDirty(const FIntBox& Bounds)
{
	VOXEL_FUNCTION_COUNTER();

	IterateResultsInBounds(
		Bounds,
		[](TVoxelSharedPtr<FVoxelSpawnerProxyResult>& Result)
		{
			if (Result.IsValid())
			{
				Result->MarkDirty();
			}
			return false;
		});
}

void FVoxelSpawnerManager::Serialize(FArchive& Ar, int32 VoxelCustomVersion)
{
	VOXEL_FUNCTION_COUNTER();

	if (Ar.IsSaving())
	{
		check(VoxelCustomVersion == FVoxelCustomVersion::LatestVersion);

		struct FGuidInfo
		{
			FVoxelSpawnerProxy* Proxy = nullptr;
			TArray<FIntVector> Positions;
			TArray<TVoxelSharedPtr<FVoxelSpawnerProxyResult>> Results;
		};
		TMap<FGuid, FGuidInfo> GuidInfos;

		// Expand groups to get SpawnerGuid -> Results
		// Storing GUIDs is more future-proof than the group/spawner index
		const auto SerializeGroups = [&](const TArray<FSpawnerGroupData>& GroupsData, const auto& Groups)
		{
			for (int32 GroupIndex = 0; GroupIndex < GroupsData.Num(); GroupIndex++)
			{
				const auto& GroupData = GroupsData[GroupIndex];
				const auto& Group = Groups[GroupIndex];

				FScopeLock Lock(&GroupData.Section);
				for (auto& It : GroupData.ChunksData)
				{
					const FIntVector& Key = It.Key;
					const FSpawnerGroupChunkData& ChunkData = It.Value;

					ensure(ChunkData.Results.Num() == 0 || ChunkData.Results.Num() == Group.Spawners.Num());
					for (int32 SpawnerIndex = 0; SpawnerIndex < ChunkData.Results.Num(); SpawnerIndex++)
					{
						const auto& Result = ChunkData.Results[SpawnerIndex];
						if (Result.IsValid() && Result->NeedsToBeSaved())
						{
							FGuidInfo& GuidInfo = GuidInfos.FindOrAdd(Group.Spawners[SpawnerIndex].Advanced.Guid);

							ensure(!GuidInfo.Proxy || GuidInfo.Proxy == GroupData.Proxies[SpawnerIndex]);
							GuidInfo.Proxy = GroupData.Proxies[SpawnerIndex];
							
							ensure(Result->Type == GuidInfo.Proxy->Type || Result->Type == EVoxelSpawnerProxyType::EmptySpawner);

							GuidInfo.Positions.Add(Key);
							GuidInfo.Results.Add(Result);
						}
					}
				}
			}
		};

		SerializeGroups(HeightGroupsData, ThreadSafeConfig.HeightGroups);
		SerializeGroups(RayGroupsData, ThreadSafeConfig.RayGroups);

		int32 NumGuid = GuidInfos.Num();
		Ar << NumGuid;

		// Now serialize each GUID
		for (auto& It : GuidInfos)
		{
			FGuid Guid = It.Key;
			FGuidInfo& GuidInfo = It.Value;

			if (!GuidInfo.Proxy || GuidInfo.Positions.Num() == 0) continue; // No data to save

			// Create one archive per GUID to be safer
			FBufferArchive GuidArchive;
			{
				int32 NumResults = GuidInfo.Positions.Num();
				check(GuidInfo.Results.Num() == NumResults);
				check(NumResults > 0);
				
				GuidArchive << NumResults;
				GuidArchive.Serialize(GuidInfo.Positions.GetData(), NumResults * sizeof(FIntVector));

				for (auto& Result : GuidInfo.Results)
				{
					ensure(Result->Type == GuidInfo.Proxy->Type || Result->Type == EVoxelSpawnerProxyType::EmptySpawner);
					GuidArchive << Result->Type;
					Result->SerializeProxy(GuidArchive, VoxelCustomVersion);
				}
			}

			Ar << Guid;

			int32 GuidArchiveNum = GuidArchive.Num();
			Ar << GuidArchiveNum;

			Ar.Serialize(GuidArchive.GetData(), GuidArchiveNum * sizeof(uint8));
		}
	}
	else
	{
		check(Ar.IsLoading());

		int32 NumGuid = -1;
		Ar << NumGuid;
		check(NumGuid >= 0);

		// Deserialize each GUID
		for (int32 GuidIndex = 0; GuidIndex < NumGuid; GuidIndex++)
		{
			FGuid Guid;
			TArray<uint8> GuidArchiveData;
			{
				Ar << Guid;

				int32 GuidArchiveNum = -1;
				Ar << GuidArchiveNum;
				check(GuidArchiveNum >= 0);

				GuidArchiveData.SetNumUninitialized(GuidArchiveNum);
				Ar.Serialize(GuidArchiveData.GetData(), GuidArchiveNum * sizeof(uint8));
			}

			int32 GroupIndex = -1;
			int32 SpawnerIndex = -1;
			bool bHeightGroup = false;

			// Find the Group Index & Spawner Index from the GUID by iterating all of them
			{
				const auto FindGUID = [&](auto& Groups)
				{
					for (int32 LocalGroupIndex = 0; LocalGroupIndex < Groups.Num(); LocalGroupIndex++)
					{
						auto& Group = Groups[LocalGroupIndex];
						for (int32 LocalSpawnerIndex = 0; LocalSpawnerIndex < Group.Spawners.Num(); LocalSpawnerIndex++)
						{
							if (Group.Spawners[LocalSpawnerIndex].Advanced.Guid == Guid)
							{
								ensureMsgf(GroupIndex == -1, TEXT("Spawners with identical GUIDs"));
								GroupIndex = LocalGroupIndex;
								SpawnerIndex = LocalSpawnerIndex;
							}
						}
					}
				};

				FindGUID(ThreadSafeConfig.HeightGroups);
				bHeightGroup = GroupIndex != -1;
				FindGUID(ThreadSafeConfig.RayGroups);
			}
			
			if (GroupIndex == -1)
			{
				UE_LOG(LogVoxel, Error, TEXT("Voxel Spawners Serialization: Loading: Spawner with GUID %s not found, skipping it"), *Guid.ToString());
				continue;
			}

			check(GroupIndex >= 0);
			check(SpawnerIndex >= 0);
			
			auto& GroupData = (bHeightGroup ? HeightGroupsData : RayGroupsData)[GroupIndex];
			auto& Proxy = *GroupData.Proxies[SpawnerIndex];
			
			FScopeLock Lock(&GroupData.Section);
			
			TArray<FIntVector> Positions;
			TArray<TVoxelSharedPtr<FVoxelSpawnerProxyResult>> Results;

			// Load GUID archive
			{
				FMemoryReader GuidArchive(GuidArchiveData);
				
				int32 NumResults = -1;
				GuidArchive << NumResults;
				check(NumResults > 0);

				Positions.SetNumUninitialized(NumResults);
				GuidArchive.Serialize(Positions.GetData(), NumResults * sizeof(FIntVector));

				Results.Reserve(NumResults);
				for (int32 ResultIndex = 0; ResultIndex < NumResults; ResultIndex++)
				{
					EVoxelSpawnerProxyType ProxyType = EVoxelSpawnerProxyType::Invalid;
					GuidArchive << ProxyType;
					check(ProxyType != EVoxelSpawnerProxyType::Invalid);

					if (ProxyType != EVoxelSpawnerProxyType::EmptySpawner && ProxyType != Proxy.Type)
					{
						UE_LOG(
							LogVoxel,
							Error,
							TEXT("Voxel Spawner Serialization: Spawner changed type, skipping it (was %s, is %s now). Type: %s; Group Index : %d; Spawner Index: %d; GUID: %s"),
							ToString(ProxyType),
							ToString(Proxy.Type),
							bHeightGroup ? TEXT("Height") : TEXT("Ray"),
							GroupIndex,
							SpawnerIndex,
							*Guid.ToString());
						break;
					}

					// Create result
					const auto Result = FVoxelSpawnerProxyResult::CreateFromType(ProxyType, Proxy);
					Result->MarkDirty();
					Result->SerializeProxy(GuidArchive, VoxelCustomVersion);
					Results.Add(Result);
				}

				if (Results.Num() != NumResults)
				{
					// Can only be caused if we broke because of the error
					continue;
				}

				ensure(GuidArchive.AtEnd());
			}
			check(Positions.Num() == Results.Num());

			// Load the results into the group data
			for (int32 ResultIndex = 0; ResultIndex < Results.Num(); ResultIndex++)
			{
				auto& ExistingResults = GroupData.ChunksData.FindOrAdd(Positions[ResultIndex]).Results;
				ExistingResults.SetNum(GroupData.Proxies.Num());
				
				auto& ExistingResult = ExistingResults[SpawnerIndex];
				
				bool bIsCreated = false;
				if (ExistingResult.IsValid())
				{
					bIsCreated = ExistingResult->IsCreated();
					if (bIsCreated)
					{
						// Destroy existing result
						ExistingResult->Destroy();
					}
					ExistingResult.Reset();
				}
				
				ExistingResult = Results[ResultIndex];

				if (bIsCreated)
				{
					// If existing result was created, create this one too
					ExistingResult->Create();
				}
			}

			GroupData.UpdateStats();
		}
	}
}

void FVoxelSpawnerManager::SaveTo(FVoxelSpawnersSave& Save)
{
	VOXEL_FUNCTION_COUNTER();

	Save.Guid = FGuid::NewGuid();

	FBufferArchive Archive;
	
	int32 VoxelCustomVersion = FVoxelCustomVersion::LatestVersion;
	Archive << VoxelCustomVersion;
	
	Serialize(Archive, FVoxelCustomVersion::LatestVersion);

	FVoxelSerializationUtilities::CompressData(Archive, Save.CompressedData);
}

void FVoxelSpawnerManager::LoadFrom(const FVoxelSpawnersSave& Save)
{
	VOXEL_FUNCTION_COUNTER();

	TArray<uint8> UncompressedData;
	FVoxelSerializationUtilities::DecompressData(Save.CompressedData, UncompressedData);

	FMemoryReader Archive(UncompressedData);

	int32 VoxelCustomVersion = -1;
	Archive << VoxelCustomVersion;
	check(VoxelCustomVersion >= 0);
	
	Serialize(Archive, VoxelCustomVersion);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelSpawnerManager::Tick(float)
{
	VOXEL_FUNCTION_COUNTER();
	
	// TODO: time limit?
	FlushCreateQueue_GameThread();
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelSpawnerManager::SpawnGroup_GameThread(FIntBox Bounds, int32 GroupIndex, bool bIsHeightGroup)
{
	VOXEL_FUNCTION_COUNTER();
	check(IsInGameThread());

	SpawnedSpawnersBounds += Bounds;

	TArray<int32> ElementsToSpawnAsync;
	TArray<TVoxelSharedPtr<FVoxelSpawnerProxyResult>> ResultsToCreateNow;
	
	auto& GroupData = (bIsHeightGroup ? HeightGroupsData : RayGroupsData)[GroupIndex];
	GroupData.Section.Lock();
	//////////////////////////////////////////////////////////////////////////////
	auto& ChunkData = GroupData.ChunksData.FindOrAdd(GroupData.GetChunkKey(Bounds));
	const FVoxelCancelCounter CancelCounter(ChunkData.UpdateIndex);
	ChunkData.Results.SetNum(GroupData.Proxies.Num());
	for (int32 ElementIndex = 0; ElementIndex < GroupData.Proxies.Num(); ElementIndex++)
	{
		const auto& Result = ChunkData.Results[ElementIndex];
		ensure(!Result.IsValid() || &Result->Proxy == GroupData.Proxies[ElementIndex]);
		if (!Result.IsValid())
		{
			ElementsToSpawnAsync.Add(ElementIndex);
		}
		else
		{
			ensure(!Result->IsCreated() || !Result->CanBeDespawned());
			if (Result->CanBeDespawned())
			{
				// If we cannot be despawned we weren't destroyed, and there is nothing to do
				ResultsToCreateNow.Add(Result);
			}
		}
	}
	GroupData.UpdateStats();
	//////////////////////////////////////////////////////////////////////////////
	GroupData.Section.Unlock();

	if (ElementsToSpawnAsync.Num() > 0)
	{
		Settings.Pool->QueueTask(
			EVoxelTaskType::FoliageBuild,
			new FVoxelSpawnerTask(*this, bIsHeightGroup, Bounds, GroupIndex, ElementsToSpawnAsync, CancelCounter));
	}

	for (auto& Result : ResultsToCreateNow)
	{
		Result->Create();
	}
}

void FVoxelSpawnerManager::DestroyGroup_GameThread(FIntBox Bounds, int32 GroupIndex, bool bIsHeightGroup)
{
	VOXEL_FUNCTION_COUNTER();

	auto& GroupData = (bIsHeightGroup ? HeightGroupsData : RayGroupsData)[GroupIndex];

	FScopeLock Lock(&GroupData.Section);
	const FIntVector ChunkKey = GroupData.GetChunkKey(Bounds);
	auto* ChunkData = GroupData.ChunksData.Find(ChunkKey);
	if (!ensure(ChunkData)) return;
	
	// Cancel existing tasks
	ChunkData->CancelTasks();
	
	// Destroy results that can be
	bool bCanFreeMemory = true;
	for (auto& Result : ChunkData->Results)
	{
		// Can be invalid if the task was started but not completed
		if (!Result.IsValid()) continue;

		if (!Result->CanBeDespawned())
		{
			// Can't do anything
			bCanFreeMemory = false;
			continue;
		}

		if (Result->IsCreated())
		{
			Result->Destroy();
		}
		else
		{
			FScopeLock CreateQueueLock(&CreateQueueSection);
			ensure(CreateQueue.RemoveSwap(Result) == 1);
		}

		if (!Result->NeedsToBeSaved())
		{
			Result.Reset();
		}
		else
		{
			bCanFreeMemory = false;
		}
	}

	// Free up memory
	if (bCanFreeMemory)
	{
		GroupData.ChunksData.Remove(ChunkKey);
	}

	GroupData.UpdateStats();
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelSpawnerManager::SpawnHeightGroup_AnyThread(const FVoxelSpawnerTask& Task)
{
	VOXEL_FUNCTION_COUNTER();
	check(Task.bIsHeightTask);

	if (Task.CancelCounter.IsCanceled()) return;
	
	const FIntBox LockedBounds = Task.Bounds.Extend(2); // For neighbors: +1; For max included vs excluded: +1
	FVoxelReadScopeLock Lock(*Settings.Data, LockedBounds, FUNCTION_FNAME);
	const FVoxelConstDataAccelerator Accelerator(*Settings.Data, LockedBounds);
	
	if (Task.CancelCounter.IsCanceled()) return;
	
	TMap<int32, TArray<FVoxelSpawnerHit>> HitsMap;
	FVoxelSpawnerUtilities::SpawnWithHeight(
		Task.CancelCounter,
		Accelerator,
		ThreadSafeConfig,
		Task.GroupIndex,
		Task.ElementsToSpawn,
		Task.Bounds,
		HitsMap);

	if (Task.CancelCounter.IsCanceled()) return;
	
	ProcessHits(Task, Accelerator, Lock, HitsMap);
}

void FVoxelSpawnerManager::SpawnRayGroup_AnyThread(const FVoxelSpawnerTask& Task)
{
	VOXEL_FUNCTION_COUNTER();

	const auto& RayGroup = ThreadSafeConfig.RayGroups[Task.GroupIndex];

	ensure(Task.Bounds.Size() == FIntVector(RENDER_CHUNK_SIZE << RayGroup.LOD));

	TArray<uint32> Indices;
	TArray<FVector> Vertices;
	Settings.Renderer->CreateGeometry_AnyThread(RayGroup.LOD, Task.Bounds.Min, Indices, Vertices);
		
	const bool bShowDebugRays = CVarShowVoxelSpawnerRays.GetValueOnAnyThread() != 0;
	const bool bShowDebugHits = CVarShowVoxelSpawnerHits.GetValueOnAnyThread() != 0;

	TUniquePtr<FVoxelSpawnerRayHandler> RayHandler;

#if USE_EMBREE_VOXEL
	RayHandler = MakeUnique<FVoxelSpawnerEmbreeRayHandler>(bShowDebugRays || bShowDebugHits, MoveTemp(Indices), MoveTemp(Vertices));
#else
	UE_LOG(LogVoxel, Error, TEXT("Embree is required for voxel spawners!"));
	return;
#endif

	if (!ensure(!RayHandler->HasError())) return;

	const FIntBox LockedBounds = Task.Bounds.Extend(2); // For neighbors: +1; For max included vs excluded: +1
	FVoxelReadScopeLock Lock(*Settings.Data, LockedBounds, FUNCTION_FNAME);
	const FVoxelConstDataAccelerator Accelerator(*Settings.Data, LockedBounds);

	TMap<int32, TArray<FVoxelSpawnerHit>> HitsMap;
	FVoxelSpawnerUtilities::SpawnWithRays(
		Task.CancelCounter,
		Accelerator,
		ThreadSafeConfig,
		Task.GroupIndex,
		Task.ElementsToSpawn,
		Task.Bounds,
		*RayHandler,
		HitsMap);

	if (bShowDebugRays || bShowDebugHits)
	{
		RayHandler->ShowDebug(Settings.VoxelWorldInterface, Task.Bounds.Min, bShowDebugRays, bShowDebugHits);
	}
	
	ProcessHits(Task, Accelerator, Lock, HitsMap);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelSpawnerManager::ProcessHits(
	const FVoxelSpawnerTask& Task,
	const FVoxelConstDataAccelerator& Accelerator,
	FVoxelReadScopeLock& Lock,
	const TMap<int32, TArray<FVoxelSpawnerHit>>& HitsMap)
{
	VOXEL_FUNCTION_COUNTER();
	
	if (Task.CancelCounter.IsCanceled()) return;

	if (CVarShowVoxelSpawnerPositions.GetValueOnAnyThread() != 0)
	{
		VOXEL_SCOPE_COUNTER("Debug Hits");
		AsyncTask(ENamedThreads::GameThread, [Hits = HitsMap, VoxelWorld = Settings.VoxelWorldInterface]()
		{
			if (VoxelWorld.IsValid())
			{
				if (UWorld* World = VoxelWorld->GetWorld())
				{
					for (auto& It : Hits)
					{
						auto& Color = GColorList.GetFColorByIndex(FMath::RandRange(0, GColorList.GetColorsNum() - 1));
						for (auto& Hit : It.Value)
						{
							auto Position = VoxelWorld->LocalToGlobalFloat(Hit.Position);
							DrawDebugPoint(World, Position, 5, Color, true, 1000.f);
							DrawDebugLine(World, Position, Position + 50 * Hit.Normal, Color, true, 1000.f);
						}
					}
				}
			}
		});
	}

	if (Task.CancelCounter.IsCanceled()) return;

	FSpawnerGroupData& GroupData = (Task.bIsHeightTask ? HeightGroupsData : RayGroupsData)[Task.GroupIndex];

	TMap<int32, TVoxelSharedPtr<FVoxelSpawnerProxyResult>> Results;
	for (int32 ElementIndex : Task.ElementsToSpawn)
	{
		if (Task.CancelCounter.IsCanceled()) return;
		
		auto& Proxy = *GroupData.Proxies[ElementIndex];

		const TArray<FVoxelSpawnerHit> EmptyHits; // Won't be in HitsMap if no hits
		auto& Hits = HitsMap.Contains(ElementIndex) ? HitsMap[ElementIndex] : EmptyHits;

		TVoxelSharedPtr<FVoxelSpawnerProxyResult> Result;
		
		if (Hits.Num() > 0)
		{
			TUniquePtr<FVoxelSpawnerProxyResult> UniqueResult = Proxy.ProcessHits(Task.Bounds, Hits, Accelerator);
			Result = TVoxelSharedPtr<FVoxelSpawnerProxyResult>(UniqueResult.Release());
			check(!Result.IsValid() || Result->Type == Proxy.Type);
		}

		if (!Result.IsValid())
		{
			// Need to create empty results that can be saved if the voxel data is modified
			Result = MakeVoxelShared<FVoxelEmptySpawnerProxyResult>(Proxy);
		}

		if (Task.bIsHeightTask)
		{
			auto& Advanced = ThreadSafeConfig.HeightGroups[Task.GroupIndex].Spawners[ElementIndex].Advanced;
			Result->SetCanBeSaved(Advanced.bSave);
			Result->SetCanBeDespawned(!Advanced.bDoNotDespawn);
		}
		else
		{
			auto& Advanced = ThreadSafeConfig.RayGroups[Task.GroupIndex].Spawners[ElementIndex].Advanced;
			Result->SetCanBeSaved(Advanced.bSave);
			Result->SetCanBeDespawned(!Advanced.bDoNotDespawn);
		}

		Results.Add(ElementIndex, Result);
	}

	// Important: Unlock before locking GroupLock to avoid deadlocks
	Lock.Unlock();

	FScopeLock GroupLock(&GroupData.Section);
	if (Task.CancelCounter.IsCanceled()) return;

	auto& ChunkData = GroupData.ChunksData.FindChecked(GroupData.GetChunkKey(Task.Bounds));
	if (!ensure(ChunkData.Results.Num() == GroupData.Proxies.Num())) return;
	
	// Atomically add to the queues and save to chunk data
	for (auto& It : Results)
	{
		ensure(!ChunkData.Results[It.Key].IsValid());
		ChunkData.Results[It.Key] = It.Value;
	}
	{
		FScopeLock ScopeLock(&CreateQueueSection);
		for (auto& It : Results)
		{
			CreateQueue.Add(It.Value);
		}
	}

	for (auto& Result : ChunkData.Results) ensure(Result.IsValid());
	ensure(!Task.CancelCounter.IsCanceled());
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelSpawnerManager::UpdateTaskCount() const
{
	Settings.DebugManager->ReportFoliageTaskCount(TaskCounter.GetValue());
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelSpawnerManager::FlushCreateQueue_GameThread()
{
	VOXEL_FUNCTION_COUNTER();
	check(IsInGameThread());

	CreateQueueSection.Lock();
	const auto QueueData = MoveTemp(CreateQueue);
	CreateQueueSection.Unlock();
	
	for (auto& Result : QueueData)
	{
		check(Result.IsValid());
		Result->Create();
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

template<typename T>
void FVoxelSpawnerManager::IterateResultsInBounds(const FIntBox& InBounds, T ApplyToResult_ReturnsShouldRebuild)
{
	VOXEL_FUNCTION_COUNTER();
	check(IsInGameThread());

	if (!SpawnedSpawnersBounds.IsValid()) return;

	// As we are iterating Bounds, we need it as small as possible
	const FIntBox Bounds = InBounds.Overlap(SpawnedSpawnersBounds.GetBox());

	const auto IterateGroups = [&](bool bHeightGroups)
	{
		auto& GroupsData = bHeightGroups ? HeightGroupsData : RayGroupsData;
		
		for (int32 GroupIndex = 0; GroupIndex < GroupsData.Num(); GroupIndex++)
		{
			auto& Group = GroupsData[GroupIndex];
			TArray<FIntBox> ChunksToRebuild;
			{
				FScopeLock Lock(&Group.Section);

				const auto KeyBounds = Bounds.MakeMultipleOfBigger(Group.ChunkSize);

				const auto IterateChunkData = [&](const FIntBox& ChunkBounds, FSpawnerGroupChunkData& ChunkData)
				{
					bool bNeedRebuild = false;
					for (auto& Result : ChunkData.Results)
					{
						if (Result.IsValid())
						{
							bNeedRebuild |= ApplyToResult_ReturnsShouldRebuild(Result);
						}
					}
					
					if (bNeedRebuild)
					{
						ChunksToRebuild.Emplace(ChunkBounds);
						// If we are rebuilding, we need to cancel existing tasks
						ChunkData.CancelTasks();
					}
				};

				// Check if it's faster to iterate ChunksData directly
				if (FIntBox(KeyBounds.Min / Group.ChunkSize, KeyBounds.Max / Group.ChunkSize).Count() < Group.ChunksData.Num())
				{
					KeyBounds.Iterate(Group.ChunkSize, [&](int32 X, int32 Y, int32 Z)
					{
						const FIntBox ChunkBounds = FIntBox(FIntVector(X, Y, Z), FIntVector(X, Y, Z) + Group.ChunkSize);
						if (auto* ChunkData = Group.ChunksData.Find(Group.GetChunkKey(ChunkBounds)))
						{
							IterateChunkData(ChunkBounds, *ChunkData);
						}
					});
				}
				else
				{
					for (auto& It : Group.ChunksData)
					{
						const FIntBox ChunkBounds(It.Key, It.Key + Group.ChunkSize);
						if (ChunkBounds.Intersect(Bounds))
						{
							IterateChunkData(ChunkBounds, It.Value);
						}
					}
				}
			}

			for (auto& Chunk : ChunksToRebuild)
			{
				SpawnGroup_GameThread(Chunk, GroupIndex, bHeightGroups);
			}
		}
	};

	IterateGroups(true);
	IterateGroups(false);
}
#endif