// Copyright 2020 Phyronnaz

#if VOXEL_PLUGIN_PRO
#include "VoxelSpawners/VoxelInstancedMeshManager.h"
#include "VoxelSpawners/VoxelHierarchicalInstancedStaticMeshComponent.h"
#include "VoxelSpawners/VoxelSpawnerActor.h"
#include "VoxelEvents/VoxelEventManager.h"
#include "VoxelData/VoxelDataAccelerator.h"
#include "VoxelGlobals.h"
#include "VoxelWorld.h"

#include "GameFramework/Actor.h"
#include "Engine/StaticMesh.h"

DECLARE_DWORD_COUNTER_STAT(TEXT("InstancedMeshManager Num Spawned Actors"), STAT_FVoxelInstancedMeshManager_NumSpawnedActors, STATGROUP_VoxelCounters);

FVoxelInstancedMeshManagerSettings::FVoxelInstancedMeshManagerSettings(
	const AVoxelWorld* World,
	const TVoxelSharedRef<IVoxelPool>& Pool,
	const TVoxelSharedRef<FVoxelEventManager>& EventManager)
	: ComponentsOwner(const_cast<AVoxelWorld*>(World))
	, NumberOfInstancesPerHISM(World->NumberOfInstancesPerHISM)
	, WorldOffset(World->GetWorldOffsetPtr())
	, Pool(Pool)
	, EventManager(EventManager)
	, CollisionDistanceInChunks(FMath::Max(0, World->SpawnersCollisionDistanceInVoxel / int32(CollisionChunkSize)))
	, VoxelSize(World->VoxelSize)
{
}

FVoxelInstancedMeshManager::FVoxelInstancedMeshManager(const FVoxelInstancedMeshManagerSettings& Settings)
	: Settings(Settings)
{
}

TVoxelSharedRef<FVoxelInstancedMeshManager> FVoxelInstancedMeshManager::Create(const FVoxelInstancedMeshManagerSettings& Settings)
{
	return MakeShareable(new FVoxelInstancedMeshManager(Settings));
}

void FVoxelInstancedMeshManager::Destroy()
{
	StopTicking();
}

FVoxelInstancedMeshManager::~FVoxelInstancedMeshManager()
{
	ensure(IsInGameThread());
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

FVoxelInstancedMeshInstancesRef FVoxelInstancedMeshManager::AddInstances(const FVoxelInstancedMeshAndActorSettings& MeshAndActorSettings, const TArray<FVoxelSpawnerMatrix>& Transforms, const FIntBox& Bounds)
{
	VOXEL_FUNCTION_COUNTER();

	if (!ensure(Transforms.Num() > 0)) return {};

	auto& Components = MeshMap.FindOrAdd(MeshAndActorSettings);
	Components.RemoveAllSwap([](auto& Component) { return !Component.IsValid(); });
	
	if (Components.Num() == 0 || 
		Components.Last()->Voxel_GetNumInstances() + Transforms.Num() > Settings.NumberOfInstancesPerHISM)
	{
		Components.Add(CreateHISM(MeshAndActorSettings));
	}
	
	UVoxelHierarchicalInstancedStaticMeshComponent* HISM = Components.Last().Get();
	if (!ensure(HISM)) return {};

	FVoxelInstancedMeshInstancesRef Ref;
	Ref.HISM = HISM;
	Ref.Section = HISM->Voxel_AppendTransforms(Transforms, Bounds);
	ensure(Ref.IsValid());
	
	return Ref;
}

void FVoxelInstancedMeshManager::RemoveInstances(FVoxelInstancedMeshInstancesRef Ref)
{
	if (!ensure(Ref.IsValid())) return;
	if (!ensure(Ref.HISM.IsValid())) return;

	Ref.HISM->Voxel_RemoveSection(Ref.Section);

	// TODO if HISM is not last check if we can still add instances to it!
}

const TArray<int32>& FVoxelInstancedMeshManager::GetRemovedIndices(FVoxelInstancedMeshInstancesRef Ref)
{
	const auto Pinned = Ref.Section.Pin();
	if (ensure(Pinned.IsValid()))
	{
		// Fine to return a ref, as Section is only used on the game thread so it won't be deleted
		return Pinned->RemovedIndices;
	}
	else
	{
		static const TArray<int32> EmptyArray;
		return EmptyArray;
	}
}

AVoxelSpawnerActor* FVoxelInstancedMeshManager::SpawnActor(
	const FVoxelInstancedMeshAndActorSettings& MeshAndActorSettings,
	FVoxelSpawnerMatrix Matrix) const
{
	VOXEL_FUNCTION_COUNTER();
	INC_DWORD_STAT(STAT_FVoxelInstancedMeshManager_NumSpawnedActors);
	
	if (!MeshAndActorSettings.ActorSettings.ActorClass) return nullptr;

	auto* ComponentsOwner = Settings.ComponentsOwner.Get();
	if (!ComponentsOwner) return nullptr;

	auto* World = ComponentsOwner->GetWorld();
	if (!World) return nullptr;

	if (World->WorldType == EWorldType::Editor || World->WorldType == EWorldType::EditorPreview) return nullptr;

	const FTransform LocalTransform(Matrix.GetCleanMatrix().ConcatTranslation(FVector(*Settings.WorldOffset) * Settings.VoxelSize));
	const FTransform GlobalTransform = LocalTransform * ComponentsOwner->GetTransform();
	
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.bDeferConstruction = true;

	auto* Actor = World->SpawnActor<AVoxelSpawnerActor>(MeshAndActorSettings.ActorSettings.ActorClass, SpawnParameters);
	if (!Actor) return nullptr;
	
	Actor->InitialLifeSpan = MeshAndActorSettings.ActorSettings.Lifespan;
	Actor->FinishSpawning(GlobalTransform);
	
	Actor->SetStaticMesh(MeshAndActorSettings.Mesh.Get(), MeshAndActorSettings.ActorSettings.BodyInstance);
	Actor->SetInstanceRandom(Matrix.GetRandomInstanceId());

	return Actor;
}

void FVoxelInstancedMeshManager::SpawnActors(
	const FVoxelInstancedMeshAndActorSettings& MeshAndActorSettings,
	const TArray<FVoxelSpawnerMatrix>& Transforms,
	TArray<AVoxelSpawnerActor*>& OutActors) const
{
	VOXEL_FUNCTION_COUNTER();
	
	for (auto& Transform : Transforms)
	{
		auto* Actor = SpawnActor(MeshAndActorSettings, Transform);
		if (!Actor) return;
		OutActors.Emplace(Actor);
	}
}

void FVoxelInstancedMeshManager::SpawnActorsInArea(
	const FIntBox& Bounds,
	const FVoxelData& Data,
	EVoxelSpawnerActorSpawnType SpawnType,
	TArray<AVoxelSpawnerActor*>& OutActors) const
{
	VOXEL_FUNCTION_COUNTER();

	const auto TransformsMap = RemoveInstancesInArea(Bounds, Data, SpawnType);

	for (auto& It : TransformsMap)
	{
		SpawnActors(It.Key, It.Value, OutActors);
	}
}

TMap<FVoxelInstancedMeshAndActorSettings, TArray<FVoxelSpawnerMatrix>> FVoxelInstancedMeshManager::RemoveInstancesInArea(
	const FIntBox& Bounds, 
	const FVoxelData& Data, 
	EVoxelSpawnerActorSpawnType SpawnType) const
{
	VOXEL_FUNCTION_COUNTER();

	const auto ExtendedBounds = Bounds.Extend(1); // As we are accessing floats, they can be between Max - 1 and Max

	TMap<FVoxelInstancedMeshAndActorSettings, TArray<FVoxelSpawnerMatrix>> TransformsMap;
	
	FVoxelReadScopeLock Lock(Data, ExtendedBounds, "SpawnActorsInArea");
	
	const TUniquePtr<FVoxelConstDataAccelerator> Accelerator =
		SpawnType == EVoxelSpawnerActorSpawnType::All
		? nullptr
		: MakeUnique<FVoxelConstDataAccelerator>(Data, ExtendedBounds);
	
	for (auto& It : MeshMap)
	{
		auto& Transforms = TransformsMap.FindOrAdd(It.Key);
		for (auto& HISM : It.Value)
		{
			if (HISM.IsValid())
			{
				HISM->Voxel_RemoveInstancesInArea(Bounds, Accelerator.Get(), SpawnType, Transforms);
			}
		}
	}

	return TransformsMap;
}

AVoxelSpawnerActor* FVoxelInstancedMeshManager::SpawnActorByIndex(UVoxelHierarchicalInstancedStaticMeshComponent* Component, int32 InstanceIndex)
{
	VOXEL_FUNCTION_COUNTER();
	
	if (!ensure(Component)) 
	{
		return nullptr;
	}
	
	FVoxelSpawnerMatrix Matrix;
	if (!Component->Voxel_RemoveInstanceByIndex(InstanceIndex, Matrix))
	{
		return nullptr;
	}
	
	FVoxelInstancedMeshAndActorSettings* ComponentSettings = nullptr;
	for (auto& It : MeshMap)
	{
		if (It.Value.Contains(Component))
		{
			ComponentSettings = &It.Key;
			break;
		}
	}
	// Only happens if this component isn't one of ours
	if (!ensure(ComponentSettings))
	{
		return nullptr;
	}

	ensure(ComponentSettings->Mesh == Component->GetStaticMesh());

	return SpawnActor(*ComponentSettings, Matrix);
}

void FVoxelInstancedMeshManager::RecomputeMeshPositions()
{
	VOXEL_FUNCTION_COUNTER();

	for (auto& It : MeshMap)
	{
		for (auto& HISM : It.Value)
		{
			SetHISMRelativeLocation(*HISM);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelInstancedMeshManager::Tick(float DeltaTime)
{
	VOXEL_FUNCTION_COUNTER();
	
	FQueuedBuildCallback Callback;
	while (HISMBuiltDataQueue.Dequeue(Callback))
	{
		auto* HISM = Callback.Component.Get();
		if (!HISM) continue;

		HISM->Voxel_FinishBuilding(*Callback.Data);
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void FVoxelInstancedMeshManager::HISMBuildTaskCallback(
	TWeakObjectPtr<UVoxelHierarchicalInstancedStaticMeshComponent> Component, 
	const TVoxelSharedRef<FVoxelHISMBuiltData>& BuiltData)
{
	HISMBuiltDataQueue.Enqueue({ Component, BuiltData });
}

UVoxelHierarchicalInstancedStaticMeshComponent* FVoxelInstancedMeshManager::CreateHISM(const FVoxelInstancedMeshAndActorSettings& MeshAndActorSettings) const
{
	VOXEL_FUNCTION_COUNTER();

	auto& MeshSettings = MeshAndActorSettings.MeshSettings;

	UE_LOG(LogVoxel, Log, TEXT("Creating a new HISM for mesh %s"), *MeshAndActorSettings.Mesh->GetPathName());

	auto* ComponentsOwner = Settings.ComponentsOwner.Get();
	if (!ComponentsOwner)
	{
		return nullptr;
	}

	UVoxelHierarchicalInstancedStaticMeshComponent* HISM;
	if (MeshSettings.HISMTemplate)
	{
		HISM = NewObject<UVoxelHierarchicalInstancedStaticMeshComponent>(ComponentsOwner, MeshSettings.HISMTemplate.Get(), NAME_None, RF_Transient);
	}
	else
	{
		HISM = NewObject<UVoxelHierarchicalInstancedStaticMeshComponent>(ComponentsOwner, NAME_None, RF_Transient);
	}

	HISM->Voxel_Init(
		Settings.Pool,
		const_cast<FVoxelInstancedMeshManager&>(*this).AsShared(),
		Settings.VoxelSize);
	HISM->Voxel_BuildDelay = MeshSettings.BuildDelay;
	HISM->bDisableCollision = true;

#define EQ(X) HISM->X = MeshSettings.X;
	EQ(bAffectDynamicIndirectLighting)
	EQ(bAffectDistanceFieldLighting)
	EQ(bCastShadowAsTwoSided)
	EQ(bReceivesDecals)
	EQ(bUseAsOccluder)
	EQ(BodyInstance)
	EQ(LightingChannels)
	EQ(bRenderCustomDepth)
	EQ(CustomDepthStencilValue)
#undef EQ
	HISM->InstanceStartCullDistance = MeshSettings.CullDistance.Min;
	HISM->InstanceEndCullDistance = MeshSettings.CullDistance.Max;
	HISM->CastShadow = MeshSettings.bCastShadow;
	HISM->bCastDynamicShadow = MeshSettings.bCastShadow;
	HISM->BodyInstance = MeshSettings.BodyInstance;
	HISM->SetCustomNavigableGeometry(MeshSettings.CustomNavigableGeometry);	
	HISM->SetStaticMesh(MeshAndActorSettings.Mesh.Get());
	HISM->SetupAttachment(ComponentsOwner->GetRootComponent(), NAME_None);
	HISM->RegisterComponent();

	SetHISMRelativeLocation(*HISM);

	if (MeshSettings.BodyInstance.GetCollisionEnabled() != ECollisionEnabled::NoCollision)
	{
		Settings.EventManager->BindEvent(
			true,
			Settings.CollisionChunkSize,
			Settings.CollisionDistanceInChunks,
			FChunkDelegate::CreateUObject(HISM, &UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_EnablePhysics),
			FChunkDelegate::CreateUObject(HISM, &UVoxelHierarchicalInstancedStaticMeshComponent::Voxel_DisablePhysics));
	}

	return HISM;
}

void FVoxelInstancedMeshManager::SetHISMRelativeLocation(UVoxelHierarchicalInstancedStaticMeshComponent& HISM) const
{
	VOXEL_FUNCTION_COUNTER();
	
	const FIntVector Position = { 0, 0, 0 };
	HISM.SetRelativeLocation(FVector(Position + *Settings.WorldOffset) * Settings.VoxelSize, false, nullptr, ETeleportType::TeleportPhysics);
}
#endif