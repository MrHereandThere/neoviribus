// Copyright 2020 Phyronnaz

#pragma once

#include "CoreMinimal.h"
#include "IntBox.h"
#include "VoxelGlobals.h"
#include "VoxelSpawner.generated.h"

class FVoxelConstDataAccelerator;
class FVoxelSpawnerManager;
class FVoxelSpawnerProxy;
class FVoxelData;
class AVoxelSpawnerActor;
class UVoxelSpawner;

#if VOXEL_PLUGIN_PRO
struct FVoxelSpawnerHit
{
	FVector Position;
	FVector Normal;

	FVoxelSpawnerHit() = default;
	FVoxelSpawnerHit(const FVector& Position, const FVector& Normal)
		: Position(Position)
		, Normal(Normal)
	{
	}
};

DECLARE_VOXEL_MEMORY_STAT(TEXT("Voxel Spawner Results Memory"), STAT_VoxelSpawnerResults, STATGROUP_VoxelMemory, VOXEL_API);

enum class EVoxelSpawnerProxyType : uint8
{
	Invalid      = 0,
	// Special spawner: any proxy can return an empty spawner
	EmptySpawner = 1,
	AssetSpawner = 2,
	MeshSpawner  = 3,
	SpawnerGroup = 4,
};

inline const TCHAR* ToString(EVoxelSpawnerProxyType ProxyType)
{
	switch (ProxyType)
	{
	case EVoxelSpawnerProxyType::Invalid: return TEXT("Invalid");
	case EVoxelSpawnerProxyType::EmptySpawner: return TEXT("EmptySpawner");
	case EVoxelSpawnerProxyType::AssetSpawner: return TEXT("AssetSpawner");
	case EVoxelSpawnerProxyType::MeshSpawner: return TEXT("MeshSpawner");
	case EVoxelSpawnerProxyType::SpawnerGroup: return TEXT("SpawnerGroup");
	default: check(false); return TEXT("");;
	}
}

class VOXEL_API FVoxelSpawnerProxyResult : public TVoxelSharedFromThis<FVoxelSpawnerProxyResult>
{
private:
	uint32 AllocatedSize = 0;
	bool bCreated = false;
	bool bDirty = false;
	bool bCanBeSaved = true;
	bool bCanBeDespawned = true;
	
public:
	const EVoxelSpawnerProxyType Type;
	const FVoxelSpawnerProxy& Proxy;

public:
	explicit FVoxelSpawnerProxyResult(EVoxelSpawnerProxyType Type, const FVoxelSpawnerProxy& Proxy);
	virtual ~FVoxelSpawnerProxyResult();

	FVoxelSpawnerProxyResult(const FVoxelSpawnerProxyResult&) = delete;
	FVoxelSpawnerProxyResult& operator=(const FVoxelSpawnerProxyResult&) = delete;

public:
	void Create();
	void Destroy();
	void SerializeProxy(FArchive& Ar, int32 VoxelCustomVersion);

	static TVoxelSharedRef<FVoxelSpawnerProxyResult> CreateFromType(EVoxelSpawnerProxyType Type, FVoxelSpawnerProxy& Proxy);

	inline bool IsCreated() const
	{
		return bCreated;
	}
	
	inline bool IsDirty() const
	{
		return bDirty;
	}
	inline void MarkDirty()
	{
		bDirty = true;
	}

	inline bool CanBeSaved() const
	{
		return bCanBeSaved;
	}
	inline void SetCanBeSaved(bool bNewCanBeSaved)
	{
		bCanBeSaved = bNewCanBeSaved;
	}

	inline bool CanBeDespawned() const
	{
		return bCanBeDespawned;
	}
	inline void SetCanBeDespawned(bool bNewCanBeDespawned)
	{
		bCanBeDespawned = bNewCanBeDespawned;
	}
	
	inline bool NeedsToBeSaved() const
	{
		return IsDirty() && CanBeSaved();
	}

protected:
	//~ Begin FVoxelSpawnerProxyResult Interface
	// Creates rendering. Called on the game thread.
	virtual void CreateImpl() = 0;
	// Destroys rendering. Called on the game thread.
	virtual void DestroyImpl() = 0;

	virtual void SerializeImpl(FArchive& Ar, int32 VoxelCustomVersion) = 0;

	virtual uint32 GetAllocatedSize() = 0;
	//~ End FVoxelSpawnerProxyResult Interface

private:
	void UpdateStats();
};

class VOXEL_API FVoxelSpawnerProxy : public TVoxelSharedFromThis<FVoxelSpawnerProxy>
{
public:
	FVoxelSpawnerManager& Manager;

	const EVoxelSpawnerProxyType Type;
	const uint32 SpawnerSeed;
	

	// Seed can be 0
	FVoxelSpawnerProxy(UVoxelSpawner* Spawner, FVoxelSpawnerManager& Manager, EVoxelSpawnerProxyType Type, uint32 Seed);
	virtual ~FVoxelSpawnerProxy() = default;

	// Both of these functions must be called only from the Manager or recursively!
	
	//~ Begin FVoxelSpawnerProxy Interface
	virtual TUniquePtr<FVoxelSpawnerProxyResult> ProcessHits(
		const FIntBox& Bounds, 
		const TArray<FVoxelSpawnerHit>& Hits,
		const FVoxelConstDataAccelerator& Accelerator) const = 0;
	virtual void PostSpawn() = 0; // Called right after every spawner is created
	//~ End FVoxelSpawnerProxy Interface
};
#endif

UCLASS(Abstract)
class VOXEL_API UVoxelSpawner : public UObject
{
	GENERATED_BODY()

public:
	// Average distance between the instances, in voxels
	// Num Instances = Area in voxels / Square(DistanceBetweenInstancesInVoxel)
	// Not a density because the values would be too small to store in a float
	UPROPERTY(EditAnywhere, Category = "General Settings", meta = (ClampMin = 0))
	float DistanceBetweenInstancesInVoxel = 10;
	
public:
#if VOXEL_PLUGIN_PRO
	virtual TVoxelSharedRef<FVoxelSpawnerProxy> GetSpawnerProxy(FVoxelSpawnerManager& Manager);
	// All added spawners MUST be valid. Returns success
	virtual bool GetSpawners(TSet<UVoxelSpawner*>& OutSpawners);
	virtual float GetDistanceBetweenInstancesInVoxel() const { return DistanceBetweenInstancesInVoxel; }
#endif
};

USTRUCT(BlueprintType, Category = Voxel)
struct VOXEL_API FVoxelSpawnersSave
{	
	GENERATED_BODY()
		
	FVoxelSpawnersSave()	= default;

	bool Serialize(FArchive& Ar);

	bool operator==(const FVoxelSpawnersSave& Other) const
	{
		return Guid == Other.Guid;
	}
	
private:
	// Version of FVoxelSpawnerSave, not of the compressed data!
	int32 Version;
	FGuid Guid;
	TArray<uint8> CompressedData;

	friend class FVoxelSpawnerManager;
};

inline FArchive& operator<<(FArchive &Ar, FVoxelSpawnersSave& Save)
{
	Save.Serialize(Ar);
	return Ar;
}

template<>
struct TStructOpsTypeTraits<FVoxelSpawnersSave> : public TStructOpsTypeTraitsBase2<FVoxelSpawnersSave>
{
	enum 
	{
		WithSerializer = true,
		WithIdenticalViaEquality = true
	};
};