// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"
#include "VoxelGlobals.h"
#include "VoxelConfigEnums.h"
#include "Templates/SubclassOf.h"
#include "VoxelSpawners/VoxelSpawnerMatrix.h"
#include "VoxelInstancedMeshSettings.h"
#include "VoxelTickable.h"

struct FVoxelInstancesSection;
struct FVoxelHISMBuiltData;
class AActor;
class AVoxelWorld;
class AVoxelSpawnerActor;
class UStaticMesh;
class UVoxelHierarchicalInstancedStaticMeshComponent;
class IVoxelPool;
class FVoxelData;
class FVoxelEventManager;
struct FIntBox;

struct VOXEL_API FVoxelInstancedMeshManagerSettings
{
	const TWeakObjectPtr<AActor> ComponentsOwner;
	const int32 NumberOfInstancesPerHISM;
	const TVoxelSharedRef<FIntVector> WorldOffset;
	const TVoxelSharedRef<IVoxelPool> Pool;
	const TVoxelSharedRef<FVoxelEventManager> EventManager;
	const uint32 CollisionChunkSize = 32; // Also change in AVoxelWorld::PostEditChangeProperty
	const uint32 CollisionDistanceInChunks;
	const float VoxelSize;

	FVoxelInstancedMeshManagerSettings(
		const AVoxelWorld* World,
		const TVoxelSharedRef<IVoxelPool>& Pool,
		const TVoxelSharedRef<FVoxelEventManager>& EventManager);
};

struct FVoxelInstancedMeshInstancesRef
{
public:
	FVoxelInstancedMeshInstancesRef() = default;

	inline bool IsValid() const
	{
		return Section.IsValid();
	}
	
private:
	TWeakObjectPtr<UVoxelHierarchicalInstancedStaticMeshComponent> HISM;
	TVoxelWeakPtr<FVoxelInstancesSection> Section;

	friend class FVoxelInstancedMeshManager;
};

class VOXEL_API FVoxelInstancedMeshManager : public FVoxelTickable, public TVoxelSharedFromThis<FVoxelInstancedMeshManager>
{
public:
	const FVoxelInstancedMeshManagerSettings Settings;

	static TVoxelSharedRef<FVoxelInstancedMeshManager> Create(const FVoxelInstancedMeshManagerSettings& Settings);
	void Destroy();
	~FVoxelInstancedMeshManager();

public:
	FVoxelInstancedMeshInstancesRef AddInstances(const FVoxelInstancedMeshAndActorSettings& MeshAndActorSettings, const TArray<FVoxelSpawnerMatrix>& Transforms, const FIntBox& Bounds);
	void RemoveInstances(FVoxelInstancedMeshInstancesRef Ref);

	static const TArray<int32>& GetRemovedIndices(FVoxelInstancedMeshInstancesRef Ref);

	AVoxelSpawnerActor* SpawnActor(
		const FVoxelInstancedMeshAndActorSettings& MeshAndActorSettings,
		FVoxelSpawnerMatrix Matrix) const;
	void SpawnActors(
		const FVoxelInstancedMeshAndActorSettings& MeshAndActorSettings,
		const TArray<FVoxelSpawnerMatrix>& Transforms,
		TArray<AVoxelSpawnerActor*>& OutActors) const;

	void SpawnActorsInArea(
		const FIntBox& Bounds,
		const FVoxelData& Data,
		EVoxelSpawnerActorSpawnType SpawnType,
		TArray<AVoxelSpawnerActor*>& OutActors) const;
	
	TMap<FVoxelInstancedMeshAndActorSettings, TArray<FVoxelSpawnerMatrix>> RemoveInstancesInArea(
		const FIntBox& Bounds,
		const FVoxelData& Data,
		EVoxelSpawnerActorSpawnType SpawnType) const;

	AVoxelSpawnerActor* SpawnActorByIndex(UVoxelHierarchicalInstancedStaticMeshComponent* Component, int32 InstanceIndex);
	
	void RecomputeMeshPositions();

protected:
	//~ Begin FVoxelTickable Interface
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickableInEditor() const override { return true; }
	//~ End FVoxelTickable Interface

public:
	void HISMBuildTaskCallback(TWeakObjectPtr<UVoxelHierarchicalInstancedStaticMeshComponent> Component, const TVoxelSharedRef<FVoxelHISMBuiltData>& BuiltData);

private:
	struct FQueuedBuildCallback
	{
		TWeakObjectPtr<UVoxelHierarchicalInstancedStaticMeshComponent> Component;
		TVoxelSharedPtr<FVoxelHISMBuiltData> Data;
	};
	TQueue<FQueuedBuildCallback> HISMBuiltDataQueue;

private:
	explicit FVoxelInstancedMeshManager(const FVoxelInstancedMeshManagerSettings& Settings);

	UVoxelHierarchicalInstancedStaticMeshComponent* CreateHISM(const FVoxelInstancedMeshAndActorSettings& MeshSettings) const;
	void SetHISMRelativeLocation(UVoxelHierarchicalInstancedStaticMeshComponent& HISM) const;

private:
	TMap<FVoxelInstancedMeshAndActorSettings, TArray<TWeakObjectPtr<UVoxelHierarchicalInstancedStaticMeshComponent>>> MeshMap;
};
#endif