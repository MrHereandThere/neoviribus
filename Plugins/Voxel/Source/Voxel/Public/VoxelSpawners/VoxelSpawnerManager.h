// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"
#include "VoxelGlobals.h"
#include "VoxelSpawnerConfig.h"
#include "Containers/Queue.h"
#include "Containers/StaticArray.h"
#include "IntBox.h"
#include "VoxelTickable.h"

class FVoxelReadScopeLock;
class FVoxelSpawnerTask;
class AVoxelWorld;
class AVoxelWorldInterface;
class FVoxelSpawnerProxy;
class FVoxelSpawnerProxyResult;
class IVoxelRenderer;
class IVoxelLODManager;
class FVoxelInstancedMeshManager;
class FVoxelEventManager;
class FVoxelDebugManager;
class FVoxelData;
class IVoxelPool;
class FVoxelConstDataAccelerator;
struct FVoxelSpawnerHit;
struct FVoxelSpawnersSave;

DECLARE_VOXEL_MEMORY_STAT(TEXT("Voxel Spawner Manager Memory"), STAT_VoxelSpawnerManagerMemory, STATGROUP_VoxelMemory, VOXEL_API);

struct FVoxelSpawnerSettings
{
	// Used for debug
	const TWeakObjectPtr<const AVoxelWorldInterface> VoxelWorldInterface;
	
	const TVoxelSharedRef<IVoxelPool> Pool;
	const TVoxelSharedRef<FVoxelDebugManager> DebugManager;
	const TVoxelSharedRef<FVoxelData> Data;
	const TVoxelSharedRef<FVoxelInstancedMeshManager> MeshManager;
	const TVoxelSharedRef<FVoxelEventManager> EventManager;
	const TVoxelSharedRef<IVoxelLODManager> LODManager;
	const TVoxelSharedRef<IVoxelRenderer> Renderer;
	const TWeakObjectPtr<UVoxelSpawnerConfig> Config;
	const float VoxelSize;
	const TMap<FName, int32> Seeds;
	const float PriorityDuration;
	
	FVoxelSpawnerSettings(
		const AVoxelWorld* World,
		const TVoxelSharedRef<IVoxelPool>& Pool,
		const TVoxelSharedRef<FVoxelDebugManager>& DebugManager,
		const TVoxelSharedRef<FVoxelData>& Data,
		const TVoxelSharedRef<IVoxelLODManager>& LODManager,
		const TVoxelSharedRef<IVoxelRenderer>& Renderer,
		const TVoxelSharedRef<FVoxelInstancedMeshManager>& MeshManager,
		const TVoxelSharedRef<FVoxelEventManager>& EventManager);
};

struct FVoxelSpawnerThreadSafeConfig
{
	EVoxelSpawnerConfigRayWorldType WorldType = EVoxelSpawnerConfigRayWorldType::Flat;
	TArray<FVoxelSpawnerConfigHeightGroup> HeightGroups;
	TArray<FVoxelSpawnerConfigRayGroup> RayGroups;
	TWeakObjectPtr<UVoxelSpawnerConfig> ConfigObject;
};

struct FVoxelCancelCounter
{
	const TVoxelSharedRef<FThreadSafeCounter64> Counter;
	const int64 Threshold;

	explicit FVoxelCancelCounter(const TVoxelSharedRef<FThreadSafeCounter64>& Counter)
		: Counter(Counter)
		, Threshold(Counter->GetValue())
	{
	}

	inline bool IsCanceled() const
	{
		return Counter->GetValue() > Threshold;
	}
};

class VOXEL_API FVoxelSpawnerManager : public FVoxelTickable, public TVoxelSharedFromThis<FVoxelSpawnerManager>
{
public:
	const FVoxelSpawnerSettings Settings;

	static TVoxelSharedRef<FVoxelSpawnerManager> Create(const FVoxelSpawnerSettings& Settings);
	void Destroy();
	~FVoxelSpawnerManager();
	
	TVoxelSharedPtr<FVoxelSpawnerProxy> GetSpawner(UVoxelSpawner* Spawner) const;

	void Regenerate(const FIntBox& Bounds);
	void MarkDirty(const FIntBox& Bounds);
	
	void Serialize(FArchive& Ar, int32 VoxelCustomVersion);

	void SaveTo(FVoxelSpawnersSave& Save);
	void LoadFrom(const FVoxelSpawnersSave& Save);

protected:
	//~ Begin FVoxelTickable Interface
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickableInEditor() const override { return true; }
	//~ End FVoxelTickable Interface

private:
	explicit FVoxelSpawnerManager(const FVoxelSpawnerSettings& Settings);

	void SpawnGroup_GameThread(FIntBox Bounds, int32 GroupIndex, bool bIsHeightGroup);
	void DestroyGroup_GameThread(FIntBox Bounds, int32 GroupIndex, bool bIsHeightGroup);

	void SpawnHeightGroup_AnyThread(const FVoxelSpawnerTask& Task);
	void SpawnRayGroup_AnyThread(const FVoxelSpawnerTask& Task);

	void ProcessHits(
		const FVoxelSpawnerTask& Task,
		const FVoxelConstDataAccelerator& Accelerator,
		FVoxelReadScopeLock& Lock,
		const TMap<int32, TArray<FVoxelSpawnerHit>>& HitsMap);

	void UpdateTaskCount() const;

	void FlushCreateQueue_GameThread();

	template<typename T>
	void IterateResultsInBounds(const FIntBox& Bounds, T ApplyToResult_ReturnsShouldRebuild);

	FIntBoxWithValidity SpawnedSpawnersBounds;

	FVoxelSpawnerThreadSafeConfig ThreadSafeConfig;
	TMap<UVoxelSpawner*, TVoxelSharedPtr<FVoxelSpawnerProxy>> SpawnersMap;

	// Tricky thread safety case: Removing a chunk whose task has been started, but not finished yet
	// To handle that, we make queuing a result into the apply queues and storing a result into the chunk data atomic
	// by locking Group.Section. Using UpdateIndex we can make sure a task isn't storing old data.
	
	struct FSpawnerGroupChunkData
	{
		// Update index, used to cancel old tasks
		TVoxelSharedRef<FThreadSafeCounter64> UpdateIndex = MakeVoxelShared<FThreadSafeCounter64>();
		// The results. Same order as Group.Proxies.
		TArray<TVoxelSharedPtr<FVoxelSpawnerProxyResult>> Results;

		inline void CancelTasks() const
		{
			UpdateIndex->Increment();
		}
	};
	struct FSpawnerGroupData
	{
		const uint32 ChunkSize = 0;
		// The group proxies, in the same order as the group spawners in ThreadSafeConfig
		const TArray<FVoxelSpawnerProxy*> Proxies;
		
		mutable FCriticalSection Section;
		TMap<FIntVector, FSpawnerGroupChunkData> ChunksData;
		mutable uint32 AllocatedSize = 0;
		
		FSpawnerGroupData(uint32 ChunkSize, TArray<FVoxelSpawnerProxy*>&& Proxies)
			: ChunkSize(ChunkSize)
			, Proxies(MoveTemp(Proxies))
		{
		}
		~FSpawnerGroupData()
		{
			DEC_VOXEL_MEMORY_STAT_BY(STAT_VoxelSpawnerManagerMemory, AllocatedSize);
		}
		
		inline FIntVector GetChunkKey(const FIntBox& Bounds) const
		{
			ensure(Bounds.Size() == FIntVector(ChunkSize));
			return Bounds.Min;
		}
		inline void UpdateStats() const
		{
			// Does not account for the Results array, but that should roughly be Number of Results * sizeof(ptr)
			// Would be too costly to compute exact size
			DEC_VOXEL_MEMORY_STAT_BY(STAT_VoxelSpawnerManagerMemory, AllocatedSize);
			AllocatedSize = sizeof(*this) + Proxies.GetAllocatedSize() + ChunksData.GetAllocatedSize();
			INC_VOXEL_MEMORY_STAT_BY(STAT_VoxelSpawnerManagerMemory, AllocatedSize);
		}
	};
	
	TArray<FSpawnerGroupData> HeightGroupsData;
	TArray<FSpawnerGroupData> RayGroupsData;

	// Can't use a TQueue as we need to remove elements
	FCriticalSection CreateQueueSection;
	TArray<TVoxelSharedPtr<FVoxelSpawnerProxyResult>> CreateQueue;

	// For debug
	mutable FThreadSafeCounter TaskCounter;
	
	friend FVoxelSpawnerTask;
};
#endif