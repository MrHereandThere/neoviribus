// Copyright 2020 Phyronnaz

#pragma once

#include "CoreMinimal.h"
#include "IntBox.h"
#include "VoxelSpawners/VoxelBasicSpawner.h"
#include "VoxelWorldGeneratorPicker.h"
#include "VoxelAssetSpawner.generated.h"

class FVoxelAssetItem;
class UVoxelAssetSpawner;
class UVoxelTransformableWorldGenerator;
class FVoxelTransformableWorldGeneratorInstance;
class FVoxelAssetSpawnerProxy;

#if VOXEL_PLUGIN_PRO
class VOXEL_API FVoxelAssetSpawnerProxyResult : public FVoxelSpawnerProxyResult
{
public:
	explicit FVoxelAssetSpawnerProxyResult(const FVoxelAssetSpawnerProxy& Proxy);

	void Init(TArray<FMatrix>&& InMatrices, TArray<int32>&& InGeneratorsIndices);

	//~ Begin FVoxelSpawnerProxyResult override
	virtual void CreateImpl() override;
	virtual void DestroyImpl() override;

	virtual void SerializeImpl(FArchive& Ar, int32 VoxelCustomVersion) override;

	virtual uint32 GetAllocatedSize() override;
	//~ End FVoxelSpawnerProxyResult override

private:
	FIntBox Bounds;
	TArray<FMatrix> Matrices;
	TArray<int32> GeneratorsIndices;
	
	TArray<TVoxelWeakPtr<FVoxelAssetItem>> Items;
};

class VOXEL_API FVoxelAssetSpawnerProxy : public FVoxelBasicSpawnerProxy
{
public:
	const TArray<TVoxelSharedPtr<FVoxelTransformableWorldGeneratorInstance>> Generators;
	const FIntBox GeneratorLocalBounds;
	const int32 Priority;
	const bool bRoundAssetPosition;
	
	FVoxelAssetSpawnerProxy(UVoxelAssetSpawner* Spawner, FVoxelSpawnerManager& Manager);
	virtual ~FVoxelAssetSpawnerProxy();

	//~ Begin FVoxelSpawnerProxy Interface
	virtual TUniquePtr<FVoxelSpawnerProxyResult> ProcessHits(
		const FIntBox& Bounds,
		const TArray<FVoxelSpawnerHit>& Hits,
		const FVoxelConstDataAccelerator& Accelerator) const override;
	virtual void PostSpawn() override {}
	//~ End FVoxelSpawnerProxy Interface
};
#endif

UCLASS()
class VOXEL_API UVoxelAssetSpawner : public UVoxelBasicSpawner
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Settings")
	FVoxelTransformableWorldGeneratorPicker Generator;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Settings")
	FIntBox GeneratorLocalBounds = FIntBox(-25, 25);

	// The voxel world seeds will be sent to the generator.
	// Add the names of the seeds you want to be randomized here
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Settings")
	TArray<FName> Seeds;

	// All generators are created at begin play
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Settings", meta = (ClampMin = 1))
	int32 NumberOfDifferentSeedsToUse = 1;

	// Priority of the spawned assets
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Settings")
	int32 Priority = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Settings")
	bool bRoundAssetPosition = false;
	
public:
	//~ Begin UVoxelSpawner Interface
#if VOXEL_PLUGIN_PRO
	virtual TVoxelSharedRef<FVoxelSpawnerProxy> GetSpawnerProxy(FVoxelSpawnerManager& Manager) override;
	//~ End UVoxelSpawner Interface
#endif
};