// Copyright 2020 Phyronnaz

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"
#include "Toolkits/IToolkit.h"

class IToolkitHost;
class UVoxelDataAsset;

class IVoxelEditorModule : public IModuleInterface
{
public:
#if VOXEL_PLUGIN_PRO
	virtual void CreateVoxelDataAssetEditor(EToolkitMode::Type Mode, const TSharedPtr<IToolkitHost>& InitToolkitHost, UVoxelDataAsset* DataAsset) = 0;
#endif

	virtual void RefreshVoxelWorlds(UObject* MatchingGenerator = nullptr) = 0;
};