// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"

struct FVoxelDataAssetData;

namespace MagicaVox
{
	bool ImportToAsset(const FString& Filename, const FString& PaletteFilename, bool bUsePalette, FVoxelDataAssetData& Asset);
}
#endif
