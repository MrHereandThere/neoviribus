// Copyright 2020 Phyronnaz

#pragma once

#if VOXEL_PLUGIN_PRO
#include "CoreMinimal.h"

struct FVoxelDataAssetData;

namespace RawVox
{
	bool ImportToAsset(const FString& File, FVoxelDataAssetData& Asset);
}

#endif
