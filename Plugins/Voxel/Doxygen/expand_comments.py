import fileinput

for line in fileinput.input():
    if not line.startswith("// Copyright"):
        shortline = line.lstrip()
        if shortline.startswith("//") and shortline[2] != "/" and shortline[2] != "~":
            line = line.replace("//", "//!", 1)
    line = line.replace("UPROPERTY", "//")
    line = line.replace("UCLASS", "//")
    line = line.replace("USTRUCT", "//")
    line = line.replace("GENERATED_BODY", "//")
    line = line.replace("UMETA", ",//")
    line = line.replace("FORCEINLINE", "")
    line = line.replace("VOXEL_API", "")
    line = line.replace("VOXELGRAPH_API", "")
    print(line, end="")